Within each folder you may find all the results of the experiments (regarding communication latency and bandwidth utilization). The file names have 6 fields separated by "-", e.g.:
proximityNeighbors-flat-500-0-4-124119
1st field: the type of the results.
2st field: organization type (hierarchical or flat).
3nd field: the number of compute nodes.
4rd field: is a proximity flag; 0 means proximity-agnostic, 1 means proximity aware.
5th field: the neighborhood size.
6th field: a unique identifier of the file.
