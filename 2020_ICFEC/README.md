Comparison of Alternative Architectures in Fog Computing
---
  
This repository contains supplementary material for a submission to ICFEC 2020 titled "Comparison of Alternative Architectures in Fog Computing". Within the folders you may find the code of a prototype implementation, a tool for performing experimental simulations, and various results acquired by performing experiments. More details can be found in the paper.

**If you are interested in hierarchical and flat fog computing architectures, follow this _tutorial_ to discover the basic functionality of this prototype, and do not hesitate to contact us for further discussion.**  
  
  
**Step 1.**  
Execute the jar file of the prototype on multiple (physical or virtual) compute nodes. Note that the jar file is configured to run on port 8001. If you want to run multiple instances on the same computer, you should run each instance on a different port.   
  
**Step 2.**  
Configure the host address of each node using a GET request:  
http://hostIP:port/putHostNodeIP?hostNodeIP=hostIP:port
  
**Step (optional).**
Configure the neighborhood size (default is 4) of each node using a GET request:  
http://hostIP:port/putGroupSize?groupSize=(integer)
  
**Step 3.**  
Configure the organization type (enum={HNP, HWP, FNP, FWP}) of each node using this GET request:  
http://hostIP:port/putOrganizationType?OrganizationType=(type)  
For hierarchical organization (i.e., HNPand HWP) set the root node using this GET request:  
http://hostIP:port/putRootNode  

**Step 4.**  
Trigger a node to join a contact using this GET request:  
http://hostIP:port/putContactNode?contactNode=contactIP:port

**Step 5.**  
Repeat 4 to add many nodes to the system.
  
**Step 6.**  
Examine the structure by checking the neighborhoods of each node using this GET request:  
http://hostIP:port/getGroupGraphs
  
**Step 7.**  
Send a message through the structure using this POST request:  
http://hostIP:port/postMessage?originalSender=(optional)  
(add the message in the body of the POST request)
  
**Step 8.**  
Check the API in order to discover more functionality. 