package core;

import java.util.ArrayList;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

import global.Global;
import utils.Util;





/**
 * @author Basil
 */
/**
 *the class that represents the object of a node (i.e., an end-device)
 *it also includes the methods for joining the overlay
 */
public class MyNode {
	/**
	 * the x coordinate of the node in a 2-dimensional space
	 */
	private double xAxis;
	/**
	 * the y coordinate of the node in a 2-dimensional space
	 */
	private double yAxis;
	/**
	 * a unique identifier of this node
	 */
	private String nodeIdentifier;
	/**
	 * a list of the graphs that include neighbors to this node
	 */
	private ArrayList<SimpleWeightedGraph<MyNode, DefaultWeightedEdge>> neighborGroups;
	/**
	 * encapsulated lists that contain neighbor nodes and their neighbors.
	 * this encapsulated list structure achieves level 1 connectivity insurance 
	 */
	private ArrayList<ArrayList<SimpleWeightedGraph<MyNode, DefaultWeightedEdge>>> 
	connectivityInsuranceL1;
	/**
	 * encapsulated lists that contain neighbor nodes and their neighbors.
	 * this encapsulated list structure achieves level 2 connectivity insurance
	 * "NOT USED IN THIS VERSION"
	 */
	private ArrayList<ArrayList<SimpleWeightedGraph<MyNode, DefaultWeightedEdge>>> 
	connectivityInsuranceL2;
	/**
	 * the id of the last message that the node received
	 */
	private int messageId;
	/**
	 * the original sender that sent the last message to this node
	 */
	private MyNode originalSender;
	/**
	 * flag that denotes if this node is disconnected
	 */
	private boolean disconnectedFlag;
	/**
	 * latency in message transmissions from broadcast until this node
	 * received the last message
	 */
	private int latencyInMessages;
	/**
	 * latency in hops from broadcast until this node
	 * received the last message	 */
	private int latencyInHops;

	/**
	 * creates a new node object
	 * @param xAxis the x coordinate of the node in a 2-dimensional space
	 * @param yAxis the y coordinate of the node in a 2-dimensional space
	 * @param nodeIdentifier a unique identifier of this node
	 */
	public MyNode(double xAxis, double yAxis, String nodeIdentifier) {
		this.xAxis=xAxis;
		this.yAxis=yAxis;
		this.nodeIdentifier=nodeIdentifier;
		this.neighborGroups=new ArrayList<SimpleWeightedGraph<MyNode, DefaultWeightedEdge>>();
		this.messageId=0;
		this.disconnectedFlag=false;
		this.connectivityInsuranceL1=new ArrayList<ArrayList<SimpleWeightedGraph<MyNode
				, DefaultWeightedEdge>>>();
		this.latencyInMessages=0;
		this.latencyInHops=0;
	}
	public double getxAxis() {
		return xAxis;
	}
	public void setxAxis(double xAxis) {
		this.xAxis = xAxis;
	}
	public double getyAxis() {
		return yAxis;
	}
	public void setyAxis(double yAxis) {
		this.yAxis = yAxis;
	}
	public String getNodeIdentifier() {
		return nodeIdentifier;
	}
	public void setNodeIdentifier(String nodeIdentifier) {
		this.nodeIdentifier = nodeIdentifier;
	}
	public ArrayList<SimpleWeightedGraph<MyNode, DefaultWeightedEdge>> 
	getNeighborGroups() {
		return neighborGroups;
	}
	public void setNeighborGroups(ArrayList<SimpleWeightedGraph<MyNode
			, DefaultWeightedEdge>> neighborGroups) {
		this.neighborGroups = neighborGroups;
	}
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public MyNode getOriginalSender() {
		return originalSender;
	}
	public void setOriginalSender(MyNode originalSender) {
		this.originalSender = originalSender;
	}
	public String toString() {
		return this.nodeIdentifier;
	}
	public boolean getDisconnectedFlag() {
		return disconnectedFlag;
	}
	public void setDisconnectedFlag(boolean disconnectedFlag) {
		this.disconnectedFlag = disconnectedFlag;
	}
	public ArrayList<ArrayList<SimpleWeightedGraph<MyNode, DefaultWeightedEdge>>> 
	getConnectivityInsuranceL1() {
		return connectivityInsuranceL1;
	}
	public void setConnectivityInsuranceL1(ArrayList<ArrayList<SimpleWeightedGraph<MyNode
			, DefaultWeightedEdge>>> connectivityInsuranceL1) {
		this.connectivityInsuranceL1 = connectivityInsuranceL1;
	}
	public ArrayList<ArrayList<SimpleWeightedGraph<MyNode, DefaultWeightedEdge>>> getConnectivityInsuranceL2() {
		return connectivityInsuranceL2;
	}
	public void setConnectivityInsuranceL2(
			ArrayList<ArrayList<SimpleWeightedGraph<MyNode, DefaultWeightedEdge>>> connectivityInsuranceL2) {
		this.connectivityInsuranceL2 = connectivityInsuranceL2;
	}
	public int getLatencyInMessages() {
		return latencyInMessages;
	}
	public void setLatencyInMessages(int latencyInMessages) {
		this.latencyInMessages = latencyInMessages;
	}
	public int getLatencyInHops() {
		return latencyInHops;
	}
	public void setLatencyInHops(int latencyInHops) {
		this.latencyInHops = latencyInHops;
	}
	/**
	 * makes this node part of a group that the groupNode belongs to
	 * @param groupNode
	 */
	public void findJoinGroup(MyNode groupNode) {
		int minGroups=groupNode.getNeighborGroups().size();
		MyNode minGroupsNode=groupNode;
		//find appropriate groupNode to join based on information from the connectivity insurance of the current groupNode
		for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupGraph : groupNode.getNeighborGroups()) {
			for(MyNode neighbor : groupGraph.vertexSet()) {
				if(neighbor.getNeighborGroups().size()<minGroups) {
					minGroups=neighbor.getNeighborGroups().size();
					minGroupsNode=neighbor;
				}else if(neighbor.getNeighborGroups().size()==minGroups) {
					int minGroupSize=minGroupsNode.getNeighborGroups().get(0).vertexSet().size();
					for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupGraphOfNeighbor : minGroupsNode.getNeighborGroups()) {
						if(groupGraphOfNeighbor.vertexSet().size()<minGroupSize) {
							minGroupSize=groupGraphOfNeighbor.vertexSet().size();
						}
					}
					for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupGraphOfNeighbor : neighbor.getNeighborGroups()) {
						if(groupGraphOfNeighbor.vertexSet().size()<minGroupSize) {
							minGroupSize=groupGraphOfNeighbor.vertexSet().size();
							minGroupsNode=neighbor;
						}
					}
				}
			}
		}
		if(minGroupsNode!=groupNode) {
			Global.joinOverhead++;//point the new node to another groupNode
			Global.joinOverhead++;//join request
		}
		this.joinGroup(minGroupsNode);
	}

	
	
	/**
	 * makes this node part of a group that the groupNode belongs to
	 * @param groupNode
	 */
	public void joinGroup(MyNode groupNode) {
		//connectivity insurance review, 
		//not every node has to share a message when the info resides in all nodes
		int groupSize=Global.groupSize;
		int joinGroupFlag=0;

		if(Global.hopAwareEnabled==0) {
			//without putting new nodes to groups in proximity	
			for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupGraph : groupNode.getNeighborGroups()) {
				if(groupGraph.vertexSet().size()<groupSize) {
					//response from groupnode overhead is added later
					this.getNeighborGroups().add(groupGraph);
					groupGraph.addVertex(this);
					for(MyNode neighbor : groupGraph.vertexSet()) {
						if(this != neighbor) {	
							DefaultWeightedEdge edge=groupGraph.addEdge(this, neighbor);
							groupGraph.setEdgeWeight(edge, Util.findShortestPathWeight(this, neighbor));
							//accept join request from groupnode and send connectivity insurance to new node
							//and send updated group to all the other neighbors 
							Global.joinOverhead++;
						}
					}
					//connectivity insurance
					for(MyNode neighborNode : groupGraph.vertexSet()) {
						if(this != neighborNode) {	
							this.getConnectivityInsuranceL1().add(neighborNode.getNeighborGroups());
							neighborNode.getConnectivityInsuranceL1().add(this.getNeighborGroups());
							if(neighborNode.getNeighborGroups().size()>1 && neighborNode!=groupNode) {
								Global.joinOverhead++;//each neighbor node sends his neighbors to the new node
							}
							if(this.getNeighborGroups().size()>1) {
								Global.joinOverhead++;//the new node sends his neighbors too (in case 
														//the new node comes from reEstablishConnection)
							}	
							for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupOfNeighborNode : neighborNode
									.getNeighborGroups()) {
								if(groupGraph != groupOfNeighborNode) {
									//to update connectivity insurance in all nodes affected by the new node
									//in other groups
									Global.joinOverhead+=groupOfNeighborNode.vertexSet().size()-1;
								}
							}
						}
					}
					for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupOfNeighborNode : this
							.getNeighborGroups()) {
						if(groupGraph != groupOfNeighborNode) {
							//to update connectivity insurance in all nodes affected by the new join
							//in other groups. only if the node joining had other groups already
							Global.joinOverhead+=groupOfNeighborNode.vertexSet().size()-1;
						}
					}
					//
					joinGroupFlag=1;
					break;
				}
			}
			if(joinGroupFlag==0){
				SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupGraph=new SimpleWeightedGraph<MyNode
						, DefaultWeightedEdge>(DefaultWeightedEdge.class); 
				groupGraph.addVertex(groupNode);
				groupGraph.addVertex(this);
				Global.joinOverhead++;//groupnode accepts join request as new group and sends 
									//connectivity insurance to new node
				DefaultWeightedEdge edge=groupGraph.addEdge(this, groupNode);
				groupGraph.setEdgeWeight(edge, Util.findShortestPathWeight(this, groupNode));
				groupNode.getNeighborGroups().add(groupGraph);
				this.getNeighborGroups().add(groupGraph);
				//connectivity insurance
				groupNode.getConnectivityInsuranceL1().add(this.getNeighborGroups());
				this.getConnectivityInsuranceL1().add(groupNode.getNeighborGroups());
				if(this.getNeighborGroups().size()>1) {
					Global.joinOverhead++;//send connectivity insurance to groupnode
				}
				for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupOfNeighborNode : groupNode
						.getNeighborGroups()) {
					if(groupGraph != groupOfNeighborNode) {
						//to update connectivity insurance in all nodes affected by the new node
						Global.joinOverhead+=groupOfNeighborNode.vertexSet().size()-1;
					}
				}
				for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupOfNeighborNode : this
						.getNeighborGroups()) {
					if(groupGraph != groupOfNeighborNode) {
						//to update connectivity insurance in all nodes affected by the new join
						//in other groups only if the node joining had other groups already
						Global.joinOverhead+=groupOfNeighborNode.vertexSet().size()-1;
					}
				}
				//
			}
		}else{
			//with putting new nodes to groups in proximity	
			SimpleWeightedGraph<MyNode, DefaultWeightedEdge> minHopGroupGraph=null;
			ArrayList<SimpleWeightedGraph<MyNode, DefaultWeightedEdge>> visitedGroups=new
					ArrayList<SimpleWeightedGraph<MyNode, DefaultWeightedEdge>>();
			for(int i=0; i<groupNode.getNeighborGroups().size();i++) {
				if(i==0) {
					Global.joinOverhead++;//groupNode sends all neighbors to new node
				}
				double hops;
				//initial value of minHop node is the groupNode
				double minHops=Util.findShortestPathWeight(this, groupNode);
				minHopGroupGraph=groupNode.getNeighborGroups().get(0);
				if(i==0) {
					//tracert to learn the number of hops from groupNode
					//+1 the response of the neighbor
					Global.joinOverhead+=minHops+1;
				}
				for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupGraph : groupNode.getNeighborGroups()) {
					if(!visitedGroups.contains(groupGraph)) {
						for(MyNode neighborNode : groupGraph.vertexSet()) {
							if(neighborNode!=groupNode) {
								hops=Util.findShortestPathWeight(this, neighborNode);
								if(i==0) {
									//tracert to learn the number of hops of each candidate neighbor
									//+1 the response of the neighbor
									Global.joinOverhead+=hops+1;
								}
								if(hops<minHops) {
									minHops=hops;
									minHopGroupGraph=groupGraph;
								}else if(hops==minHops) {
									if(groupGraph.vertexSet().size()<minHopGroupGraph.vertexSet().size()) {
										minHopGroupGraph=groupGraph;
									}
								}
							}
						}
					}
				}
				minHopGroupGraph.addVertex(this);
				Global.joinOverhead+=2;//to get the current groupgraph
				for(MyNode neighborNode : minHopGroupGraph.vertexSet()) {
					if(this != neighborNode) {	
						Global.joinOverhead++;//to send the  updated groupgraph to each groupnode
											//plus connectivity insurance if any(it had other neighbors already)
						DefaultWeightedEdge edge=minHopGroupGraph.addEdge(this, neighborNode);
						minHopGroupGraph.setEdgeWeight(edge, Util.findShortestPathWeight(this, neighborNode));
					}
				}
				MyNode maxHopsNode=null;
				if(minHopGroupGraph.vertexSet().size() <= groupSize){
					this.getNeighborGroups().add(minHopGroupGraph);
					//connectivity insurance
					for(MyNode neighborNode : minHopGroupGraph.vertexSet()) {
						if(this != neighborNode) {	
							this.getConnectivityInsuranceL1().add(neighborNode.getNeighborGroups());
							neighborNode.getConnectivityInsuranceL1().add(this.getNeighborGroups());
							if(neighborNode.getNeighborGroups().size()>1) {
								Global.joinOverhead++;//each neighbor node sends his neighbors to the new node
							}
							//if(this.getNeighborGroups().size()>1) {
							//	Global.joinOverhead++;//the new node sends his neighbors too (in case 
														//the new node already had other neighbors)
														//added earlier
							//}	
							for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupOfNeighborNode : neighborNode
									.getNeighborGroups()) {
								if(minHopGroupGraph != groupOfNeighborNode) {
									//to update connectivity insurance in all nodes affected by the new node
									//in other groups
									Global.joinOverhead+=groupOfNeighborNode.vertexSet().size()-1;
								}
							}
						}
					}
					for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupOfNeighborNode : this
							.getNeighborGroups()) {
						if(minHopGroupGraph != groupOfNeighborNode) {
							//to update connectivity insurance in all nodes affected by the new join
							//in other groups only if the node joining had other groups already
							Global.joinOverhead+=groupOfNeighborNode.vertexSet().size()-1;
						}
					}
					//
					joinGroupFlag=1;
					break;
				}else {
					double sumHops;
					double maxSumHops=0;
					for(MyNode destinationNode : minHopGroupGraph.vertexSet()) {
						if(this!=destinationNode) {
							maxSumHops+=Util.findShortestPathWeight(this, destinationNode);
						}
					}
					maxHopsNode=this;
					for(MyNode sourceNode : minHopGroupGraph.vertexSet()) {
						if(sourceNode!=this) {
							sumHops=0;
							for(MyNode destinationNode : minHopGroupGraph.vertexSet()) {
								if(sourceNode!=destinationNode) {
									sumHops+=Util.findShortestPathWeight(sourceNode, destinationNode);
								}
							}
							if(sumHops>maxSumHops) {//what if there are many nodes with the same maxSumHops
								maxSumHops=sumHops;//but do not fix because initially maxSumHops is the new node
								maxHopsNode=sourceNode;
							}
						}
					}
				}
				if(maxHopsNode==groupNode) {
					minHopGroupGraph.removeVertex(groupNode);
					groupNode.getNeighborGroups().remove(minHopGroupGraph);
					this.getNeighborGroups().add(minHopGroupGraph);
					joinGroupFlag=1;
					//connectivity insurance
					for(MyNode neighborNode : minHopGroupGraph.vertexSet()) {
						if(this != neighborNode) {
							this.getConnectivityInsuranceL1().add(neighborNode.getNeighborGroups());
							neighborNode.getConnectivityInsuranceL1().add(this.getNeighborGroups());
							if(neighborNode.getNeighborGroups().size()>1) {
								Global.joinOverhead++;//each neighbor node sends his neighbors to the new node
							}
							//if(this.getNeighborGroups().size()>1) {
							//	Global.joinOverhead++;//the new node sends his neighbors too (in case 
														//the new node already had other neightbors)
														//added earlier
							//}	
							for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupOfNeighborNode : neighborNode
									.getNeighborGroups()) {
								if(minHopGroupGraph != groupOfNeighborNode) {
									//to update connectivity insurance in all nodes affected by the new node
									//in other groups
									Global.joinOverhead+=groupOfNeighborNode.vertexSet().size()-1;
								}
							}
						}
						if(neighborNode!=maxHopsNode) {
							neighborNode.getConnectivityInsuranceL1().remove(maxHopsNode.getNeighborGroups());
							maxHopsNode.getConnectivityInsuranceL1().remove(neighborNode.getNeighborGroups());
							//joinOverhead is not affected because each node can remove without notification
						}
					}
					for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupOfNeighborNode : this
							.getNeighborGroups()) {
						if(minHopGroupGraph != groupOfNeighborNode) {
							//to update connectivity insurance in all nodes affected by the new join
							//in other groups only if the node joining had other groups already
							Global.joinOverhead+=groupOfNeighborNode.vertexSet().size()-1;
						}
					}
					//
					this.findJoinGroup(groupNode);//because the node this has the group that groupNode is the
											//maxhopNode. so better have "this" join "groupnode" than in reverse
					break;
				}else if(maxHopsNode!=this) {
					minHopGroupGraph.removeVertex(maxHopsNode);
					maxHopsNode.getNeighborGroups().remove(minHopGroupGraph);
					this.getNeighborGroups().add(minHopGroupGraph);
					joinGroupFlag=1;
					//connectivity insurance
					for(MyNode neighborNode : minHopGroupGraph.vertexSet()) {
						if(this != neighborNode) {	
							this.getConnectivityInsuranceL1().add(neighborNode.getNeighborGroups());
							neighborNode.getConnectivityInsuranceL1().add(this.getNeighborGroups());
							if(neighborNode.getNeighborGroups().size()>1) {
								Global.joinOverhead++;//each neighbor node sends his neighbors to the new node
							}
							//if(this.getNeighborGroups().size()>1) {
							//	Global.joinOverhead++;//the new node sends his neighbors too (in case 
														//the new node already had other neightbors)
														//added earlier
							//}	
							for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupOfNeighborNode : neighborNode
									.getNeighborGroups()) {
								if(minHopGroupGraph != groupOfNeighborNode) {
									//to update connectivity insurance in all nodes affected by the new node
									//in other groups
									Global.joinOverhead+=groupOfNeighborNode.vertexSet().size()-1;
								}
							}
						}
						if(neighborNode!=maxHopsNode) {
							neighborNode.getConnectivityInsuranceL1().remove(maxHopsNode.getNeighborGroups());
							maxHopsNode.getConnectivityInsuranceL1().remove(neighborNode.getNeighborGroups());
							//joinOverhead is not affected because each node can remove without notification
						}
					}
					for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupOfNeighborNode : this
							.getNeighborGroups()) {
						if(minHopGroupGraph != groupOfNeighborNode) {
							//to update connectivity insurance in all nodes affected by the new join
							//in other groups only if the node joining had other groups already
							Global.joinOverhead+=groupOfNeighborNode.vertexSet().size()-1;
						}
					}
					//
					this.findJoinGroup(maxHopsNode);//because the node this has the group that maxHopsNode is the
													//maxhopNode. so better have "this" join "maxHopsNode" than in reverse
					break;
				}
				minHopGroupGraph.removeVertex(this);
				visitedGroups.add(minHopGroupGraph);
			}
			if(joinGroupFlag==0){
				SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupGraph=new SimpleWeightedGraph<MyNode
						, DefaultWeightedEdge>(DefaultWeightedEdge.class);
				groupGraph.addVertex(groupNode);
				groupGraph.addVertex(this);
				Global.joinOverhead+=Util.findShortestPathWeight(this, groupNode)+1;// groupNode finds hop distance
				Global.joinOverhead++;//groupnode accepts join request as new group and sends 
									//connectivity insurance and groupGraph to new node
				DefaultWeightedEdge edge=groupGraph.addEdge(this, groupNode);
				groupGraph.setEdgeWeight(edge, Util.findShortestPathWeight(this, groupNode));
				groupNode.getNeighborGroups().add(groupGraph);
				this.getNeighborGroups().add(groupGraph);
				//connectivity insurance
				groupNode.getConnectivityInsuranceL1().add(this.getNeighborGroups());
				this.getConnectivityInsuranceL1().add(groupNode.getNeighborGroups());
				if(this.getNeighborGroups().size()>1) {
					Global.joinOverhead++;//send connectivity insurance to groupnode
				}
				for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupOfNeighborNode : groupNode
						.getNeighborGroups()) {
					if(groupGraph != groupOfNeighborNode) {
						//to update connectivity insurance in all nodes affected by the new node
						Global.joinOverhead+=groupOfNeighborNode.vertexSet().size()-1;
					}
				}
				for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> groupOfNeighborNode : this
						.getNeighborGroups()) {
					if(groupGraph != groupOfNeighborNode) {
						//to update connectivity insurance in all nodes affected by the new join
						//in other groups
						Global.joinOverhead+=groupOfNeighborNode.vertexSet().size()-1;
					}
				}
				//	
			}
		}
	}

	public void reEstablishConnection(MyNode disconnectedNode) {
		ArrayList<MyNode> reconnectNodes=new ArrayList<MyNode>(); 
		for(ArrayList<SimpleWeightedGraph<MyNode, DefaultWeightedEdge>> neighborGroupList 
				: this.getConnectivityInsuranceL1()) {
			for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> neighborGroup : neighborGroupList) {
				if(neighborGroup.vertexSet().contains(disconnectedNode) ) {
					neighborGroup.removeVertex(disconnectedNode); //overhead is added later
					for(MyNode node : neighborGroup.vertexSet()) {
						if(node!=disconnectedNode) {
							//check if node is connected
							Global.reEstablishConnectionOverhead++;
							if(!node.getDisconnectedFlag()) {
								//respond 
							//	Global.reEstablishConnectionOverhead++;
								reconnectNodes.add(node);
								//notify neighbors of the disconnected node
								Global.reEstablishConnectionOverhead+=neighborGroup.vertexSet().size()-1;
								//neighbors remove connectivity insurance of disconnected node
								for(MyNode neighbor : neighborGroup.vertexSet()) {
									neighbor.getConnectivityInsuranceL1().remove(disconnectedNode.getNeighborGroups());	
								}
								break;
							}else {
								//here neighbors of other disconnected nodes can be notified
								//but at this version we assume detection of disconnected nodes only
								//when a neighbor tries to send a message. additional changes might be
								//required if detection takes place here
							}
						}
					}
				}
			}
		}
		for(MyNode reconnectNode : reconnectNodes) {
			//overhead of join request
			Global.reEstablishConnectionOverhead++; 
			this.findJoinGroup(reconnectNode);
		}
	}
	public void resetConnectivity() {
		this.messageId=0;
		this.disconnectedFlag=false;
		this.connectivityInsuranceL1=new ArrayList<ArrayList<SimpleWeightedGraph<MyNode
				, DefaultWeightedEdge>>>();
		for(SimpleWeightedGraph<MyNode, DefaultWeightedEdge> group : this.neighborGroups) {
			group.removeVertex(this);
		}
		this.neighborGroups=new ArrayList<SimpleWeightedGraph<MyNode, DefaultWeightedEdge>>();
		this.originalSender=null;
	}



}
