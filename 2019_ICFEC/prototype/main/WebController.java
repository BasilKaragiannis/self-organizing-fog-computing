package main;


import java.util.Map;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import global.Variables;
import threads.JoinRequest;
import threads.SendMessage;
import threads.UpdateConnectivityInsurance;
import threads.GlobalBroadcast;
import threads.CreateGroupGraph;
import threads.UpdateGroupGraph;
import utils.Util;

	/**this class contains all the resources of the API
	 * @author Basil
	 *
	 */
	@CrossOrigin(origins = "*")
	@RestController
	@Configuration
	@PropertySource("classpath:application.properties")
	public class WebController {
		
		/**
		 * triggers the node to join the network through the contactNode
		 * @param contactNode the address of the nodes used as contact 
		 * @return
		 */
		@RequestMapping(method = RequestMethod.GET, value = "/putContactNode")
		public ResponseEntity<String> putContactNode(@RequestParam Map<String, 
				String> customQuery){
			new Thread(new JoinRequest(customQuery.get("contactNode"))).start();
			return new ResponseEntity<String>(HttpStatus.OK);
		}
		
		/**return empty response in order for the sender to measure
		 * RTT latency
		 * @return
		 * 
		 * The system call to traceroute is implemented in Utils
		 */
		@RequestMapping(method = RequestMethod.GET, value="/checkLatency")
	    public ResponseEntity<String> checkLatency() {
/*	    	try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}*/
	    	return new ResponseEntity<>(HttpStatus.OK);
	    }
		
	    /**return all the neighbors of this node (comma separated)
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value="/getAllNeighbours")
	    public ResponseEntity<String> getAllNeighbourNodes() {
	    	String allNeighbourNodes="";
	    	for(SimpleDirectedWeightedGraph<String,DefaultWeightedEdge> groupGraph 
	    			: Variables.groupGraphs ) {
	    		for(String neighbourNode : groupGraph.vertexSet()) {
	    			allNeighbourNodes=allNeighbourNodes+neighbourNode+",";
	    		} 
	    	}
	    	return new ResponseEntity<>(allNeighbourNodes, HttpStatus.OK);
	    }
	    
	    /**return the group that contains GroupNode
	     * @param GroupNode the address of a neighbor node
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value="/getGroupEdges")
	    public ResponseEntity<String> getGroupNodes(@RequestParam Map<String, 
	    		String> customQuery){
	    	for(SimpleDirectedWeightedGraph<String,DefaultWeightedEdge> groupGraph 
	    			: Variables.groupGraphs ) {
	    		if(groupGraph.vertexSet().contains(customQuery.get("containsGroupNode"))) {
	    			return new ResponseEntity<>(Util.convertGraphEdgesToString(groupGraph), 
	    					HttpStatus.OK);
	    		}
	    	}
	    	return new ResponseEntity<>("", HttpStatus.OK);
	    }
	    
	    /**return the latency RTT from this node to targetNode
	     * @param targetNode the address of any node (in the network) 
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value="/requestLatencyCheck")
	    public ResponseEntity<String> requestLatencyCheck(@RequestParam Map<String, 
	    		String> customQuery) {	    	
	    	return new ResponseEntity<>(String.valueOf(Util.findLatency(customQuery.
	    			get("targetNode"))), HttpStatus.OK);
	    }
	    
	    /**update an existing groupGraph
	     * @param groupEdgesString string representation of a group
	     * @param contactNode the address of a node that belong in the group
	     * of groupEdgesString
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value = "/putGroupGraph")
		public ResponseEntity<String> putGroupGraph(@RequestParam Map<String, 
				String> customQuery){
	    	new Thread(new UpdateGroupGraph(customQuery.get("groupEdgesString"), 
	    			customQuery.get("contactNode"))).start();
	    	return new ResponseEntity<String>(HttpStatus.OK);
	    }
	    
	    /**create new groupGraph
	     * @param groupEdgesString string representation of the new group to be
	     * created
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value = "/postGroupGraph")
		public ResponseEntity<String> postGroupGraph(@RequestParam Map<String, 
				String> customQuery){
	    	new Thread(new CreateGroupGraph(customQuery.get("groupEdgesString"))).start();
	    	return new ResponseEntity<String>(HttpStatus.OK);
	    }
	    
	    /**return all the group graphs of this node
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value = "/getGroupGraphs")
		public ResponseEntity<String> getGroupGraphs(){
	    	String allGroupGraphEdges="";
	    	for (SimpleDirectedWeightedGraph<String,DefaultWeightedEdge> groupGraph :
	    		Variables.groupGraphs) {
	    		allGroupGraphEdges=allGroupGraphEdges+Util.convertGraphEdgesToString(
	    				groupGraph)+"\n";
	    	}
	    	return new ResponseEntity<String>(allGroupGraphEdges, HttpStatus.OK);
	    }
	    
	    /**update the value of groupSize
	     * @param groupSize the maximum number of nodes in a group
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value = "/putGroupSize")
		public ResponseEntity<String> putGroupSize(@RequestParam Map<String, 
				String> customQuery){
	    	Variables.groupSize=Integer.valueOf(customQuery.get("groupSize"));
	    	return new ResponseEntity<String>(HttpStatus.OK);
	    }
	    
	    /**update the address of this node
	     * @param customQuery
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value = "/putHostNodeIP")
		public ResponseEntity<String> putHostNodeIP(@RequestParam Map<String, 
				String> customQuery){
	    	Variables.hostNodeIP=customQuery.get("hostNodeIP");
	    	return new ResponseEntity<String>(HttpStatus.OK);
	    }
	    
	    /**terminate the app on this node (used for debugging)
	     * @param customQuery
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value = "/terminateApp")
		public ResponseEntity<String> terminateApplication(@RequestParam Map<String, 
				String> customQuery){
	    	System.exit(0);
	    	return new ResponseEntity<String>(HttpStatus.OK);
	    }

	    /**retutn this node's address
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value = "/getHostNodeIP")
		public ResponseEntity<String> getHostNodeIP(){
	    	return new ResponseEntity<String>(Variables.hostNodeIP, HttpStatus.OK);
	    }
	    
	    /**return the maximum group size of this node
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value = "/getGroupSize")
		public ResponseEntity<String> getGroupSize(){
	    	return new ResponseEntity<String>(String.valueOf(Variables.groupSize),
	    			HttpStatus.OK);
	    }
	    
	    /**broadcast a message to all the nodes of the network
	     * @param message string message to be broadcast to all nodes in the
	     * group graphs
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value = "/broadcast")
		public ResponseEntity<String> broadcast(@RequestParam Map<String, 
				String> customQuery){
	    	new Thread(new GlobalBroadcast(customQuery.get("message"))).start();

	    	return new ResponseEntity<String>(HttpStatus.OK);
	    }

	    /**send a message to a list of receivers in the same group 
	     * @param message the message to be sent
	     * @param receivers a list of receivers of the message
	     * @param originalSender the address of the original sender of this message
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value = "/sendMessage")
		public ResponseEntity<String> sendMessage(@RequestParam Map<String, 
				String> customQuery){
	    	new Thread(new SendMessage(customQuery.get("message"),
	    			customQuery.get("receivers"), customQuery.get("originalSender")
	    			)).start();
	    	return new ResponseEntity<String>(HttpStatus.OK);
	    }
	    
	    
	    /**Update the connectivity insurance information used to recover form 
	     * failure
	     * @param nodeAddress the address of the node which updates the 
	     * connectivity insurance
	     * @param groupGraphs the updated connectivity insurance information
	     * @return
	     */
	    @RequestMapping(method = RequestMethod.GET, value = 
	    		"/postConnectivityInsurance")
		public ResponseEntity<String> postConnectivityInsurance(@RequestParam 
				Map<String, String> customQuery){
	    	new Thread(new UpdateConnectivityInsurance(customQuery.
	    			get("nodeAddress"), customQuery.get("groupGraphs"))).start();
	    	return new ResponseEntity<String>(HttpStatus.OK);
	    }


	}