package global;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import core.ConnectivityInsurance;

import java.util.ArrayList;


/**this class contains globally public variables
 * @author Basil
 *
 */ 
public class Variables {
////////////////////////////joining//////////////////////////////////////////
	/**the IP of the hostnode
	 * 
	 */
	public static String hostNodeIP="xxx.xxx.xxx.xxx:xxxx";	
	
	/**the size of the groups (system parameter)
	 * 
	 */
	public static int groupSize=3;
	
	/**a list of all the group graphs that the node belongs to
	 * 
	 */
	public static ArrayList<SimpleDirectedWeightedGraph
	<String,DefaultWeightedEdge>>groupGraphs=new ArrayList
	<SimpleDirectedWeightedGraph<String,DefaultWeightedEdge>>();
	
	/**the nodes of the group that the hostNode belonged to before trying
	 *to rejoin for being the most distant. this is used for the joining procedure
	 */
	public static ArrayList<String> formerGroupNodes=new ArrayList<String>();
	
////////////////////////////messaging/////////////////////////////////////////
	/**
	 * a message field
	 */
	public static String message;
	
	/**
	 * the original sender of the message
	 */
	public static String originalSender;
	
///////////////////////////fault tolerance///////////////////////////////////
	/**list of ConnectivityInsurance objects. this is used for maintaining
	 * connectivity when unresponsive nodes appear
	 */
	public static ArrayList<ConnectivityInsurance> connectivityInsurance=
			new ArrayList<ConnectivityInsurance>();
}
