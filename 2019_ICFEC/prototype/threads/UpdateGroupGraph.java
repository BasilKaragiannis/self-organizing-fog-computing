package threads;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import global.Variables;
import utils.Util;

/**this thread updates an existing group graph
 * @author Basil
 *
 */
public class UpdateGroupGraph  implements Runnable {
	
	/**String value that contains all the edges of a graph (source, target, weight)
	 * 
	 */
	String groupEdgesString;
	
	/**the contact of the new node that caused the update
	 * 
	 */
	String contactNode;
	
	/**public constructor
	 * @param groupEdgesString String value that contains all the edges of a graph 
	 * (source, target, weight)
	 * @param contactNode the contact of the new node that caused the update
	 */
	public UpdateGroupGraph(String groupEdgesString, String contactNode){
		this.groupEdgesString=groupEdgesString;
		this.contactNode=contactNode;
	}

	@Override
	public void run() {
		//create graph out of groupEdgesString
		SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> updatedGroupGraph;
		updatedGroupGraph=Util.convertGraphEdgesStringToGraph(groupEdgesString);
		//find a neighbour node inside the updatedGroupGraph
		String neighbour=(String) updatedGroupGraph.vertexSet().toArray()[0];
		if(neighbour.equals(Variables.hostNodeIP)) {
			neighbour=(String) updatedGroupGraph.vertexSet().toArray()[1];
		}
		//find the group from Variables.groupGraphs that contains the neighbour
		//and replace it by the updatedGroupGraph
		for(int i=0;i<Variables.groupGraphs.size();i++) {
			if(Variables.groupGraphs.get(i).vertexSet().contains(neighbour)) {
				Variables.groupGraphs.set(i, updatedGroupGraph);
				break;
			}
		}
		//if the size of the updated graph is greater that the maximum group size
		if(updatedGroupGraph.vertexSet().size()>Variables.groupSize) {
			//find most distant node
			String mostDistantNode=Util.findMostDistantNode(updatedGroupGraph);
			//remove most distant node
			updatedGroupGraph.removeVertex(mostDistantNode);
			//if the most distant node is this node
			if(mostDistantNode.equals(Variables.hostNodeIP)) {
				//the most distant node leaves the group
				Variables.groupGraphs.remove(updatedGroupGraph);
				Variables.formerGroupNodes.addAll(updatedGroupGraph.vertexSet());
				//the most distant node joins the network through the contact
				new Thread(new JoinRequest(contactNode)).start();
			}
		}
	}
}
