package threads;


import java.util.ArrayList;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;


import global.Variables;
import utils.Util;

/**this thread performs all the actions needed for a node to join the 
 * network as a self-organizing compute node
 * @author Basil
 *
 */
public class JoinRequest  implements Runnable {
	/**another node that implements this protocol which is used as contact
	 * 
	 */
	String contactNode;	
	
	/**public constructor
	 * @param contactNode another node that implements this protocol which
	 * is used as contact
	 */
	public JoinRequest(String contactNode){
		this.contactNode=contactNode;
	}

	@Override
	public void run() {
		int joinGroupFlag=0;
		//get all neighbours from the contactNode
		ArrayList<String> contactNodeNeighbours=Util.
				convertCommaSeparatedStringToArrayList(Util.sendGetRequest("http://"+
						contactNode+"/getAllNeighbours"));
		//remove neighbours from previous attempts to join the contactNode
		contactNodeNeighbours.removeAll(Variables.formerGroupNodes);
		//if the contactNode has neighbours
		if(contactNodeNeighbours.size()>1) {
			//while there are neighbours in the list
			while(contactNodeNeighbours.size()!=0 && joinGroupFlag==0) {
				int[] neighbourLatencies=new int[contactNodeNeighbours.size()];
				//find latency to each neighbour
				for(int i=0;i<neighbourLatencies.length;i++) {
					neighbourLatencies[i]=Util.findLatency(contactNodeNeighbours.get(i));
				}
				int minLatency=neighbourLatencies[0];
				int minLatencyIndex=0;
				//find neighbour with minimum latency
				for(int i=1;i<neighbourLatencies.length;i++) {
					if(neighbourLatencies[i]<minLatency) {
						minLatency=neighbourLatencies[i];
						minLatencyIndex=i;
					}
				}
				//get the groupGraph of the node with the minimum latency
				SimpleDirectedWeightedGraph<String,DefaultWeightedEdge> groupGraph;
				String groupEdgesString=Util.sendGetRequest("http://"+contactNode+
						"/getGroupEdges?containsGroupNode="+contactNodeNeighbours.get
						(minLatencyIndex));
//				String test;
				groupGraph=Util.convertGraphEdgesStringToGraph(groupEdgesString);
				groupGraph.addVertex(Variables.hostNodeIP);
				//find latency to each group node and from each group node
				for(String groupNode : groupGraph.vertexSet()) {
					if(!groupNode.equals(Variables.hostNodeIP)) {
						DefaultWeightedEdge edgeTo=groupGraph.addEdge(Variables.
								hostNodeIP, groupNode);
						groupGraph.setEdgeWeight(edgeTo, Util.findLatency(groupNode));	

						DefaultWeightedEdge edgeFrom=groupGraph.addEdge(groupNode, 
								Variables.hostNodeIP);
						groupGraph.setEdgeWeight(edgeFrom, Double.valueOf(Util.
								sendGetRequest("http://"+groupNode+
										"/requestLatencyCheck?targetNode="+Variables.
										hostNodeIP)));
					}
				}
				String mostDistantNode=Util.findMostDistantNode(groupGraph);
				if(groupGraph.vertexSet().size()<=Variables.groupSize ||
						!mostDistantNode.equals(Variables.hostNodeIP)) {
					//update graph in all groupNodes
					Util.updateGroupGraphInNeighbours(groupGraph, contactNode);
					//if groupGraph.size>groupSize remove most distant noce
					if(groupGraph.vertexSet().size()>Variables.groupSize) {
						groupGraph.removeVertex(mostDistantNode);
					}
					//add new group to groupGraphs
					Variables.groupGraphs.add(groupGraph);
					//empty the list of former neighbour nodes
					Variables.formerGroupNodes.clear();
					//raise joinGroupFlag
					joinGroupFlag=1;
				//else if group is full and hostNode is the most distant node
				}else {
					//remove group nodes from contactNodeNeighbours and find 
					//another group
					contactNodeNeighbours.removeAll(groupGraph.vertexSet());
				}
				//if the contactNode does not have neighbours
			}
		}
		//no group has been joined so far, which mean that a new group will be created
		if(joinGroupFlag==0){
			//create empty graph, add hostNode and contactNode
			SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> newGroupGraph=new 
					SimpleDirectedWeightedGraph<String, 
					DefaultWeightedEdge>(DefaultWeightedEdge.class);
			Variables.groupGraphs.add(newGroupGraph);
			newGroupGraph.addVertex(Variables.hostNodeIP);
			newGroupGraph.addVertex(contactNode);
			//find latency to and from the contact node
			DefaultWeightedEdge edgeTo=newGroupGraph.addEdge(Variables.hostNodeIP,
					contactNode);
			newGroupGraph.setEdgeWeight(edgeTo, Util.findLatency(contactNode));	
			DefaultWeightedEdge edgeFrom=newGroupGraph.addEdge(contactNode,
					Variables.hostNodeIP);
			newGroupGraph.setEdgeWeight(edgeFrom, Double.valueOf
					(Util.sendGetRequest("http://"+
					contactNode+"/requestLatencyCheck?targetNode="+Variables
					.hostNodeIP)));
			//update groupGraph in contactNode
			String newGroupEdgesString=Util.convertGraphEdgesToString(newGroupGraph);
			Util.sendGetRequest("http://"+contactNode+"/postGroupGraph?"+
					"groupEdgesString="+newGroupEdgesString);	
		}		
	}
}
