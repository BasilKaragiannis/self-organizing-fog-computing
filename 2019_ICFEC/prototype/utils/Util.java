package utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import core.ConnectivityInsurance;
import global.Variables;
import threads.JoinRequest;

/**
 * @author Basil
 *
 */
public class Util {

	/**sends a GET request to targetUrl
	 * @param targetUrl the address of the get request
	 * @return the response of the get request
	 */
	public static String sendGetRequest(String targetUrl) {
		StringBuffer response = new StringBuffer();
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL(targetUrl).
					openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Type", "x-www-form-urlencoded");
			connection.setConnectTimeout(3000);
			if (connection.getResponseCode() != 200) {
				throw new RuntimeException("GET Request to "+ targetUrl +
						" failed with Error code : "
						+ connection.getResponseCode());
			}
			BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
					(connection.getInputStream())));
			String output;
			while((output=responseBuffer.readLine()) != null){
				response.append(output+"\n");
			}   
			connection.disconnect();
		}catch (Exception e) {
			System.out.println(e.getMessage());
			return "connectionTimeout";
		}
		return response.toString();
	}

	/**converts a String representation of a graph to a graph object
	 * @param graphEdgesString a string representation of a graph
	 * @return a graph object that corresponds to the string parameter
	 */
	public static SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> 
	convertGraphEdgesStringToGraph(String graphEdgesString){
		SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> graph=new 
				SimpleDirectedWeightedGraph<String, 
				DefaultWeightedEdge>(DefaultWeightedEdge.class);
		//split the string
		String[] edges=graphEdgesString.split(",");
		for(int i=0;i<edges.length;i++) {
			try {//there is an empty cell after split;
				//when the string is received from GET 
				//thats why this is done inside a try/catch
				String edgeSource=edges[i].split("-")[0]; 
				String edgeTarget=edges[i].split("-")[1];
				String edgeWeight=edges[i].split("-")[2];
				//add the fields from the string to the graph object
				graph.addVertex(edgeSource);
				graph.addVertex(edgeTarget);
				DefaultWeightedEdge edge=graph.addEdge(edgeSource, edgeTarget);
				graph.setEdgeWeight(edge, Double.valueOf(edgeWeight));
			}catch(Exception e) {}
		}
		return graph;
	}

	/**measures the latency RTT between this node and targetNode
	 * @param targetNode the address of the target node
	 * @return the latency RTT in milliseconds
	 */
	public static int findLatency(String targetNode) {
		long startTimer = System.nanoTime();
		Util.sendGetRequest("http://"+targetNode+"/checkLatency");
		long stopTimer = System.nanoTime();
		long timeElapsed = stopTimer-startTimer;
		return (int)(timeElapsed/1000000); //convert to milliseconds
	}

	/**convert a graph object to a string
	 * @param graph object
	 * @return the string representation of the graph object
	 */
	public static String convertGraphEdgesToString(
			SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> graph) {
		String graphEdgesString="";
		for(DefaultWeightedEdge edge : graph.edgeSet()) {	
			graphEdgesString=graphEdgesString+
					graph.getEdgeSource(edge)+"-"+
					graph.getEdgeTarget(edge)+"-"+
					graph.getEdgeWeight(edge)+",";
		}
		return graphEdgesString;
	}
	
	/**converts a list of graphs to a string
	 * @param graphsList a list of graphs
	 * @return a string representation of the list of graphs
	 */
	public static String convertListOfGraphsToString(
			ArrayList<SimpleDirectedWeightedGraph<String, 
			DefaultWeightedEdge>> graphsList) {
		String graphsString="";
		for(SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> graph 
				: graphsList) {	
			graphsString=graphsString+Util.convertGraphEdgesToString(graph)+"!";
		}
		return graphsString;
	}
	
	/**converts a string to a list of graphs
	 * @param graphsString a string representaion of a list of graphs
	 * @return an Arraylist of graphs objects that correspond to the graphsString
	 */
	public static ArrayList<SimpleDirectedWeightedGraph<String,
	DefaultWeightedEdge>>convertStringToListOfGraphs(String graphsString) {
		ArrayList<SimpleDirectedWeightedGraph<String,DefaultWeightedEdge>>graphsList=
				new ArrayList<SimpleDirectedWeightedGraph<String
				,DefaultWeightedEdge>>();
		String[] graphs=graphsString.split("!");
		for(int i=0;i<graphs.length;i++) {
			try {//there is an empty cell after split;
				//when the string is received from GET 
				//thats why this is done inside a try/catch
				graphsList.add(Util.convertGraphEdgesStringToGraph(graphs[i]));
			}catch(Exception e) {}
		}
		return graphsList;
	}

	/**finds the node in a graph that has the maximum sum of weights of
	 * incoming edges
	 * @param graph the graphs to be examined
	 * @return the most distant node
	 */
	public static String findMostDistantNode(SimpleDirectedWeightedGraph
			<String, DefaultWeightedEdge> graph) {
		double maxIncomingWeightSum=0;
		String maxIncomingWeightSumNode="";
		for(String node : graph.vertexSet()) {
			double incomingWeightSum=0;
			for(DefaultWeightedEdge edge : graph.incomingEdgesOf(node)) {
				incomingWeightSum=incomingWeightSum+graph.getEdgeWeight(edge);
			}
			if(incomingWeightSum>maxIncomingWeightSum) {
				maxIncomingWeightSum=incomingWeightSum;
				maxIncomingWeightSumNode=node;
			}
		}
		return maxIncomingWeightSumNode;
	}

	/**converts a String of comma separated values into an Arraylist of values
	 * @param commaSeparatedString a string of comma seperated values
	 * @return an ArrayList object witht the values of commaSeparatedString
	 */
	public static ArrayList<String> convertCommaSeparatedStringToArrayList(
			String commaSeparatedString){
		ArrayList<String> valuesList=new ArrayList<String>();
		String[] valuesTable=commaSeparatedString.split(",");
		for(int i=0;i<valuesTable.length-1;i++) {//last cell is empty
			valuesList.add(valuesTable[i]);
		}
		return valuesList;
	}
	
	/**converts a list into a String of comma separated values
	 * @param arrayList a list of values
	 * @return a string of comma separated values from arrayList
	 */
	public static String convertArrayListToCommaSeparatedString(
			ArrayList<String> arrayList){
		String string="";
		for(int i=0;i<arrayList.size()-1;i++) {
			string=arrayList.get(i)+",";
		}
		return string;
	}
	
	/**updates a group graph in all the nodes of the group
	 * @param groupGraph the group graph to be updated
	 * @param contactNode the contact node that led to this update
	 * used for to distinguishing the group from the other groups
	 * @return void
	 */
	public static void updateGroupGraphInNeighbours(SimpleDirectedWeightedGraph
			<String, DefaultWeightedEdge> groupGraph, String contactNode) {
		for(String groupNode : groupGraph.vertexSet()) {
			if(!groupNode.equals(Variables.hostNodeIP)) {
				String updatedGroupEdgesString=Util.convertGraphEdgesToString(
						groupGraph);
				Util.sendGetRequest("http://"+groupNode+"/putGroupGraph?"+
						"groupEdgesString="+updatedGroupEdgesString+
						"&contactNode="+contactNode);
			}
		}	
	}
	
	/**updates the connectivity insurance in all the neighbors
	 * @return void
	 */
	public static void updateConnectivityInsuranceInNeighbours() {
		for(SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> group
				: Variables.groupGraphs) {
			for(String groupNode : group.vertexSet()) {
				Util.sendGetRequest("http://"+groupNode+
						"/postConnectivityInsurance?"+
						"nodeAddress="+Variables.hostNodeIP+
						"&groupGraphs="+Util.convertListOfGraphsToString(
								Variables.groupGraphs));
			}
		}
	}
	
	/**makes a system call to traceroute in order to find hop count
	 * @param targetNode the address of a node in the network
	 * @return the number of hops
	 */
	public int findHops(String targetNode) {
		String os = System.getProperty("os.name").toLowerCase();
		String output="";
		try {
			Process traceRt;
			if(os.contains("win")) {
				traceRt=Runtime.getRuntime().exec("tracert "+targetNode+
						" -w 1000 -h 100");
			}else{
				traceRt=Runtime.getRuntime().exec("traceroute "+targetNode+
						" -I -w 1 -m 100");
			}
			// read the output from the command
			BufferedInputStream bis = new BufferedInputStream(traceRt.
					getInputStream());
			ByteArrayOutputStream buf = new ByteArrayOutputStream();
			int result = bis.read();
			while(result != -1) {
				buf.write((byte) result);
				result = bis.read();
			}
			// StandardCharsets.UTF_8.name() > JDK 7
			output=buf.toString("UTF-8");

			//read any errors from the attempted command
			//String errors = convertStreamToString(traceRt.getErrorStream());
			//if(errors != "") LOGGER.error(errors);
		}catch(Exception e){
			System.out.println("error while performing trace route command");
		}
		//find the hop count in the traceroute output
		String[] lines=output.split("\n");
		String line=lines[lines.length-4]; //last line that has the hop count
		String hops=line.substring(0, 4);  //first characters is the hop count

		return Integer.valueOf(hops);
	}

	/**
	 * broadcasts a message to all the groups apart from the group of the
	 * original sender
	 * @param message the message to be broadcast
	 */
	public static void globalBroadcast(String message) {
		for(int i=0;i<Variables.groupGraphs.size();i++) {
			if(!Variables.groupGraphs.get(i).vertexSet().contains(Variables.
					originalSender)) {
				Util.groupBroadcast(message, Variables.groupGraphs.get(i));
			}
		}
	}

	/**
	 * broadcasts a message to all the neighbors of a group graph
	 * @param message the message to be broadcast
	 * @param groupGraph the group to which the message will be broadcast.
	 */
	public static void groupBroadcast(String message, SimpleDirectedWeightedGraph
			<String, DefaultWeightedEdge> groupGraph) {
		ArrayList<String> receivers=new ArrayList<String>();
		for(String node : groupGraph.vertexSet()) {
			if(node!=Variables.hostNodeIP) {
				receivers.add(node);
			}
		}
		Util.groupMulticast(message, receivers, groupGraph);
	}	

	/**the fault tolerance mechanism. this node join each one of the groups
	 * of the unresponsive node
	 * @param unrNode a node that does not respond to HTTP requests
	 */
	public static void reEstablishConnection(String unrNode) {
		for(ConnectivityInsurance con : Variables.connectivityInsurance) {
			if(con.getNodeAddress().equals(unrNode)) {
				for(SimpleDirectedWeightedGraph<String, DefaultWeightedEdge>
				groupGraph  : con.getGroupGraphs()) {
					if(!groupGraph.vertexSet().contains(Variables.hostNodeIP)) {
						groupGraph.removeVertex(unrNode);
						new Thread(new JoinRequest((String) groupGraph.vertexSet()
								.toArray()[0])).start();
					}
				}
			}
		}	
	}
	
	/**
	 * sends a message to the nodes of the list "receivers" based on
	 * a weight minimisation logic. the nodes of the list and senderNode
	 * all belong to the same group
	 * @param message the message to be sent
	 * @param receivers a list of the destinations of the multicast
	 */
	public static void groupMulticast(String message, ArrayList<String> receivers
			, SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> groupGraph) {
		if(receivers.remove(Variables.hostNodeIP)){
			//this node is a receiver of the message not just a liaison
			Variables.message=message;
		}
		//to deactivate intelligent messaging
		/*if(Global.hopAwareEnabled==0) {
			//without hop minimization
			for(MyNode receiverNode : receivers) {
				//send message
				Global.broadcastMessages++;
				Global.broadcastHops+=Util.findShortestPathWeight(senderNode, receiverNode);
				if(receiverNode.getDisconnectedFlag()) {
					groupGraph.removeVertex(receiverNode);
					//notify neighbors of the disconnected node
					Global.reEstablishConnectionOverhead+=groupGraph.vertexSet().size()-1;
					senderNode.reEstablishConnection(receiverNode);
					for(MyNode node : groupGraph.vertexSet()) {
						//upon notification every node removes connectivity insurance without extra messaging
						node.getConnectivityInsuranceL1().remove(receiverNode.getNeighborGroups());
					}
				}else {
					//send response
					//	Global.broadcastMessages++;
					//	Global.broadcastHops+=Util.findShortestPathWeight(senderNode, receiverNode);
					if(receiverNode.getMessageId()==senderNode.getMessageId()) {
						Global.redundantMessages++;
					}
					receiverNode.setMessageId(senderNode.getMessageId());
					receiverNode.setOriginalSender(senderNode);
					receiverNode.setLatencyInMessages(senderNode.getLatencyInMessages()+1);
					receiverNode.setLatencyInHops(senderNode.getLatencyInHops()+(int)Util.findShortestPathWeight
							(senderNode, receiverNode));
					Util.broadcast(receiverNode);
				}
			}
		}else {*/
		//with hop minimization
		//in tempGraph,the vertices are references to the actual nodes, 
		//thus do not modify. the edges are newly generated thus,
		//do modify at will
		SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> tempGraph=
				new SimpleDirectedWeightedGraph<String, DefaultWeightedEdge>
		(DefaultWeightedEdge.class);
		for(String node : groupGraph.vertexSet()) {
			tempGraph.addVertex(node);
		}
		for(DefaultWeightedEdge edge : groupGraph.edgeSet()) {
			DefaultWeightedEdge tempEdge=tempGraph.addEdge(groupGraph.
					getEdgeSource(edge), groupGraph.getEdgeTarget(edge));
			tempGraph.setEdgeWeight(tempEdge, groupGraph.getEdgeWeight(edge));
		}
		ArrayList<ArrayList<String>> sendPaths=new ArrayList<ArrayList<String>>();
		DijkstraShortestPath<String, DefaultWeightedEdge> dijkstra=
				new DijkstraShortestPath<String, DefaultWeightedEdge>(tempGraph);
		for(int i=0;i<receivers.size();i++) {
			double minShortestPath=Double.POSITIVE_INFINITY;
			double currentShortestPath=0;
			String shortestPathNode=null;
			for(String node : receivers) {
				currentShortestPath=dijkstra.getPathWeight(Variables.hostNodeIP,
						node);
				if(currentShortestPath<minShortestPath && currentShortestPath>0){
					minShortestPath=currentShortestPath;
					shortestPathNode=node;
				}
			}
			sendPaths.add((ArrayList<String>)dijkstra.getPath(Variables.
					hostNodeIP, shortestPathNode).getVertexList());
			for(DefaultWeightedEdge edge : dijkstra.getPath(Variables.hostNodeIP,
					shortestPathNode).getEdgeList()) {
				//maybe something very small but greater than zero,
				//so that it also achieves min latency
				tempGraph.setEdgeWeight(edge, 0);
			}
		}
		ArrayList<String> oneHopNodes=new ArrayList<String>();
		ArrayList<ArrayList<String>> sendPathsReduced=new ArrayList<ArrayList
				<String>>();
		for(ArrayList<String> path : sendPaths) {
			if(!oneHopNodes.contains(path.get(1))) {
				oneHopNodes.add(path.get(1));
			}
		}
		for(String node : oneHopNodes) {
			ArrayList<String> pathReduced=new ArrayList<String>();
			pathReduced.add(node);
			sendPathsReduced.add(pathReduced);
		}
		for(ArrayList<String> path : sendPaths) {
			for(ArrayList<String> pathReduced : sendPathsReduced) {
				if(path.get(1)==pathReduced.get(0)) {
					pathReduced.add(path.get(path.size()-1));
				}				
			}
		}
		for(ArrayList<String> sendPath : sendPathsReduced) {
			//the first node of the list is the next hop receiver
			String receiverNode=sendPath.remove(0);
			//send message within the group to the next hop receiver
			String sendResponse=Util.sendGetRequest("http://"+receiverNode+
					"/sendMessage?message="+Variables.message+
					"&receivers="+convertArrayListToCommaSeparatedString(sendPath)+
					"&originalSender="+Variables.hostNodeIP);
			//if the node is unresponsive
			if(sendResponse.equals("connectionTimeout")) {
				//in case the receiverNode was also a destination not just a liaison
				sendPath.remove(receiverNode);
				groupGraph.removeVertex(receiverNode);
				//notify neighbors of the disconnected node
				Util.updateGroupGraphInNeighbours(groupGraph, Variables.hostNodeIP);
				Util.reEstablishConnection(receiverNode);	
				//calculate another path to reach the nodes in "sendPath"
				Util.groupMulticast(message, sendPath, groupGraph);
			}else {

			}
		}
	}
}
