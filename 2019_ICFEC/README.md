Self-Organizing Compute Nodes Project Repository  
---

This repository contains the code used in the development of an experimental prototype for Self-Organizing Compute Nodes.
The prototype is developed with: Spring Tool Suite 3.9.4 using Spring Boot 2.0 and JgraphT 1.2.0.
The evaluation code is developed using: Eclipse Java EE IDE Version: Oxygen 4.7.0

**If you are interested in Self-Organizing Compute Nodes follow this _tutorial_ to find out some basic functionality which can serve as a starting point.**  
  
  
**Step 1.**  
Deploy the prototype application on multiple compute nodes.  
  
**Step 2.**  
Configure the host IP addresses in all nodes using this GET request:  
_<hostAddress>:<port>/putHostNodeIP?hostNodeIP=<hostAddress>:<port>_
  
**Step 3.**  
Configure the maximum group size in all nodes using this GET request:  
_<hostAddress>:<port>/putGroupSize?groupSize=<integer value>_
  
**Step 4.**  
Have a node join a contact node in the network using this GET request:  
_<hostAddress>:<port>/putContactNode?contactNode=<contactAddress>:<port>_
  
**Step 5.**  
Repeat 4 to add many nodes.
  
**Step 6.**  
Check the neighbors of each node using this GET request:  
_<hostAddress>:<port>/getAllNeighbours_
  
**Step 7.**  
Check the groups of each node using this GET request:  
_<hostAddress>:<port>/getGroupGraphs_
  
**Step 8.**  
Check out the code to discover more functionalities