Context-Aware Routing in Fog Computing Systems
---
  
This repository contains supplementary material for a submission to IEEE Transactions on Cloud Computing, titled "Context-Aware Routing in Fog Computing Systems". Within the folder "prototype" you may find the code of a prototype implementation, and various results acquired by performing experiments using geographically distributed compute nodes. Within the folder "simulator" you may find a python-based simulator, and various results we have acquired for our evaluation. Details regarding the setup can be found in the paper.