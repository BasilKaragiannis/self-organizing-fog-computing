"""
Q-learning algorithm implementation. 

Performs e-greedy exploration. 
loop() is the main function that initializes the Q-table and the topology and executes this algorithm,
as well as two alternatives for comparison. Parameters (e.g., topology) can be loaded from a json file
and passed on as the first parameter to loop(). For examples on how to run it, see main.py.
"""

from itertools import product
from pprint import pprint
import random
import numpy
import sys
import time
import copy
import math
import getopt

def generate_topology(params):
  """Generate a random topology based on the specs included in params"""

  node_latencies = [0]*params["num_compute_nodes"]
  shortcut_node_latencies = [0]*params["num_compute_nodes"]
  
  retval = copy.deepcopy(params)

  path_latency = 90
  for i in range(params["num_compute_nodes"]):
    # create random latency in the range [10, 100]
    L = random.randint(10, 12)
    node_latencies[i] = path_latency + L
    path_latency += L
    # except for 1st node, direct latency to node i should be sth smaller than its node latency (from 75%-90%)
    if i == 0:
      shortcut_node_latencies[i] = node_latencies[i]
    else:
      shortcut_node_latencies[i] = (1 - i*0.04)*node_latencies[i]
  retval["node_latencies"] = node_latencies
  retval["shortcut_node_latencies"] = shortcut_node_latencies

  return retval
 
def init_nodes(params):
  """Initialize node list.

  For each node, we maintain the following values:
  - Probability to accept a request.
  - Latency to be reached directly from gateway
  """
  N = []
  for i in range(0, params["num_compute_nodes"]):
    latency = params["shortcut_node_latencies"][i]
    if i < params["num_compute_nodes"] - 1:
      if "accept_probabilities" in params:
        accept_probability = params["accept_probabilities"][i]
      else:
        accept_probability = params["init_node_accept_probability"]

      if "temp_accept_probabilities" in params:
        temp_accept_probability = params["temp_accept_probabilities"][i]
      else:
        temp_accept_probability = params["temp_node_accept_probability"]

      next_hop_latency = params["node_latencies"][i + 1] - params["node_latencies"][i]
    else:
      # the last node always accepts
      accept_probability = 1.0
      temp_accept_probability = 1.0
      next_hop_latency = 0.0

    # keep also track of when a node last accepted a request
    N.append({"latency": latency, "next_hop_latency": next_hop_latency, "accept_probability": accept_probability, "init_accept_probability": accept_probability, "temp_accept_probability": temp_accept_probability, "requests_served": 0, "last_request_timestamp": 0})
  return N

def init_qtable(num_compute_nodes, k, initq = 0):
  """Initialize Q table for use with the e-greedy algorithm using minimal state overhead."""
  node_ids = range(0, num_compute_nodes)
  Q = numpy.ndarray(shape=(num_compute_nodes**k, num_compute_nodes), dtype=numpy.float32)
  Q.fill(initq)
  action_counters = numpy.ndarray(shape=(num_compute_nodes**k, num_compute_nodes), dtype=numpy.int32)
  Q.fill(initq)
  action_counters.fill(0)

  return Q,action_counters

def select_action(Q, state, params):
  """Select an action using an epsilon-greedy policy.

  With probability epsilon, explore (pick a random state). Otherwise, exploit (select
  the action with the highest Q-value. Returns the action and its value.
  """

  """
  # limit action space to only observable actions, i.e., 1st node and all other nodes in the history.
  elements = state.split("-")
  if not "0" in elements:
    elements.append("0")
  """
  state = state_to_idx(state, len(Q[0]))

  if random.random() < params["epsilon"]:
    # explore
    action = random.randint(0, params["num_compute_nodes"]-1)
    #print("Exploring: Choice ", action, ", epsilon = ", params["epsilon"])
    #time.sleep(0.3)    
    value = Q[state][action]
  else:
    # exploit
    #print("Exploiting from ", elements)
    actions = Q[state]
    action = -1
    value = 0
    counter = 0
    # argmax(actions from given state)
    for a in actions:
      # limit the actions only to nodes "visible" from the gateway
      #if str(counter) not in elements:
      #  counter += 1
      #  continue

      if action < 0: 
        value = a
        action = 0
      elif value < a:
        value = a
        action = counter
      counter += 1
    #print("Exploiting: Choice ", action, ", available actions:", actions)
  return action, value

def select_action_boltzmann(Q, state, T):
  """Select an action using a softmax exploration strategy.
  """

  #T = max(0.1, T)
  pi = [0]*len(Q[state])
  s = 0
  for i in range(0, len(Q[state])):
    s += math.exp(Q[state][i]/T)

  #print("T = ", T, ", s = ", s, ", state len: ", len(Q[state]), ", state is ", Q[state])  
  for i in range(0,len(Q[state])):
    pi[i] = math.exp(Q[state][i]/T)/s
    
  action = numpy.random.choice(len(Q[state]), 1, p=pi)
  return action[0], Q[state][action[0]]

def observe_reward(N, action, params):
  """Get the reward that corresponds to this action and possibly reconfigure serving node.

  The reward is the -total_latency when action (aka the node id) is selected. To calculate it,
  we simulate the behavior of a node hierarchy as follows:
  - We get the latency from the gateway to the selected node.
  - If it does not accept the request, we pass on the request to the next.
  - The latency to pass on the request is latency(next) - latency(current)
  (- When a node accepts the request, we can add up a randomly drawn computation delay and return.)
  """
  num_nodes = params["num_compute_nodes"]

  # latency to reach first node
  total_latency = numpy.random.normal(N[action]["latency"], N[action]["latency"]/20.0)
  #total_latency = N[action]["latency"]

  selected = num_nodes - 1
  for i in range(action, num_nodes):
    # break if a node accepts
    rnd = numpy.random.random()
    #print("Flipped: ", rnd, ", accept probability of node ", i, " is ", N[i]["accept_probability"])
    if rnd > N[i]["accept_probability"]: # node did not accept, push fwd
      total_latency += numpy.random.normal(N[i]["next_hop_latency"], N[i]["next_hop_latency"]/20.0)
      #total_latency += N[i]["next_hop_latency"]
    else:
      selected = i
      N[i]["requests_served"] += 1
      break
  return -total_latency, selected

def normalize_reward(r):
  return max(0, 1-r/450.0)

def update_accept_probabilities(N, params):
  """Go over all nodes and with some probability, reset them."""
  for i in range(0, params["num_compute_nodes"] - 1):
    if random.random() < params["node_reset_probability"]:
      N[i]["accept_probability"] = N[i]["init_accept_probability"]

def update_by_inactivity(N, params, t):
  T = 5 # some sort of default inactivity threshold
  if "inactivity_threshold" in params:
    T = params["inactivity_threshold"]

  for i in range(0, params["num_compute_nodes"]):
    if t - N[i]["last_request_timestamp"] > T:
      # node has been inactive for a while-->reset it
      N[i]["accept_probability"] = N[i]["init_accept_probability"]

def reset_nodes(N, params):
  """Go over all nodes and reset them."""
  for i in range(0, params["num_compute_nodes"]-1):
    N[i]["accept_probability"] = N[i]["init_accept_probability"]

def setup_random_accept_probabilities(params):
  temp_accept_probabilities = [1.0]*params["num_compute_nodes"]
  for i in range(0, params["num_compute_nodes"]-1):
    temp_accept_probabilities[i] = random.uniform(params["init_node_accept_probability"], 1.0)
  params["temp_accept_probabilities"] = temp_accept_probabilities

def update_state(state, action):
  """Update the current state based on action. 
  """
  # parse state
  history = state.split('-')
  
  # replace most recent action, pop out oldest action
  history.pop(0)
  history.append(action)

  # re-generate state id
  new_state = ""
  for h in history:
    new_state += str(h) + "-"

  return new_state[:-1]

def update_qtable(Q, s1, s2, action, reward, params):
  """Update the Q-table based on the states, action, observed reward and learning params.
  """
  alpha = params["learning_rate"]
  gamma = params["discount_factor"]

  # find action that maximizes future returns starting from the new state (s2)
  actions = Q[s2]
  theaction = -1
  value = 0
  counter = 0
  # argmax(actions from given state)
  for a in actions:
    if theaction < 0: 
      value = a
      theaction = 0
    elif value < a:
      value = a
      theaction = counter
    counter += 1
  Q[s1][action] = Q[s1][action] + alpha * (reward + gamma * value - Q[s1][action]) 
  #print(actions)
  #print("Updating qtable at state ", s1, " - action ", action, " with: a = ", alpha, reward, gamma, value)
  #time.sleep(0.01)

###################################
###################################
#  functions use different state representation (2d numpy matrix of 16-bit floats) to save memory

def state_to_idx(state, num_nodes):
  """Converts a state ID to its index in the Q-table.

  Works as follows: 0-0-...-0 maps to 0. 1-0-...-0 maps to 1. 
  If we have n nodes, a state x-y-..-z maps to x*n0 + y*n^1 + ... + z*n^k
  """
  retval = 0
  ids = state.split("-")
  ids.reverse()
  for i in range(0, len(ids)):
    retval += int(ids[i])*(num_nodes**i)
  return retval

def loop(params, show_averages=True, output_per_round=False, verbose=False):
  """ RL loop.

  After initializing the Q-table and state, it loops "forever" (for each new request of the app)
  - Exploration vs. exploitation: Based on the current state, decides on an action (where to send the request)
  - Observes the reward of the action (-latency).
  - Updates Q-table and moves to new state.

  show_averages: Print/return current average rather than current reward sample
  output_per_round: Return all samples if true, only the average for each strategy otherwise
  verbose: output data at each round
  """
  # initialize Q table
  Q, action_counters = init_qtable(params["num_compute_nodes"], params["history_size"], initq=0)

  # initialize node array
  N = init_nodes(params)

  # Keep separate node arrays for other strategies so that their accept probabilities are not messed up
  N_traditional = init_nodes(params)
  N_lightweight = init_nodes(params)

  # select initial state as 0-0-0...
  # since the default for the gw is to send all requests to the first compute node
  # in the chain.
  state = ""
  for i in range(0, params["history_size"]):
    state += "0" + "-"
  state = state[:-1]

  # sum of rewards  
  sumR = 0.0
  sumR_traditional = 0.0
  sumR_lightweight = 0.0

  # how many requests/time steps/rounds to keep going
  iterations = params["iterations"]

  # for calculating the mean latency for the last "window" requests
  divby = 0
  window = iterations

  last_served_by = 0

  # for boltzmann exploration, if used (TODO: put somewhere else)
  T = 10000

  # return values
  samples_ql = []
  samples_traditional = []
  samples_lightweight = []
 
  # loop forever
  for i in range(0,iterations):
    # with some probability, each node is reset -- typically set to zero so that we have no random resets.
    update_accept_probabilities(N, params)
    update_accept_probabilities(N_traditional, params)
    update_accept_probabilities(N_lightweight, params)

    # inactive nodes have to be reset
    update_by_inactivity(N, params, i)
    update_by_inactivity(N_traditional, params, i)
    update_by_inactivity(N_lightweight, params, i)

    # select an action
    # uncomment for e-greedy exploration
    action, qvalue = select_action(Q, state, params)

    # uncomment for softmax exploration
    #action, qvalue = select_action_boltzmann(Q, state_to_idx(state, params["num_compute_nodes"]), T)
    #print("Selected action: ", action)
    #T*=0.95
    
    # update action counters
    state_id = state_to_idx(state, params["num_compute_nodes"])
    action_counters[state_id][action] += 1
    alpha = 1/(action_counters[state_id][action]) # update learning rate for state-action pair
    params["learning_rate"] = alpha


    # observe reward
    r, selected = observe_reward(N, action, params)
    reward = normalize_reward(-r)

    N[selected]["accept_probability"] = N[selected]["temp_accept_probability"]
    N[selected]["last_request_timestamp"] = i

    # update qtable
    new_state = update_state(state, selected)

    state_id = state_to_idx(state, params["num_compute_nodes"])
    new_state_id = state_to_idx(new_state, params["num_compute_nodes"])

    update_qtable(Q, state_id, new_state_id, action, reward, params)

    # update current state
    state = new_state

    ##############################################
    # Default strategy: always send the to the first hop and observe latency
    r_traditional, selected = observe_reward(N_traditional, 0, params)  
    r_traditional *= -1
    N_traditional[selected]["accept_probability"] = N_traditional[selected]["temp_accept_probability"]
    N_traditional[selected]["last_request_timestamp"] = i

    ##############################################
    # lightweight context aware strategy (no learning, keep history of last request)
    #if random.random() < 0.5:
    #if i%2 == 0:
    if i%(params["inactivity_threshold"]+1) == 0:
      # select random non-cloud node or switch to traditional strategy
      last_served_by = 0
    r_lightweight, last_served_by = observe_reward(N_lightweight, last_served_by, params)
    N_lightweight[last_served_by]["accept_probability"] = N_lightweight[last_served_by]["temp_accept_probability"]
    N_lightweight[last_served_by]["last_request_timestamp"] = i
    r_lightweight *= -1

    params["epsilon"] *= 0.999 

    sumR += r
    sumR_traditional += r_traditional
    sumR_lightweight += r_lightweight
    divby += 1

    if show_averages:
      # keep track of reward values (either the current sample values or the current average)
      samples_ql.append(-sumR/divby)
      samples_traditional.append(sumR_traditional/divby)
      samples_lightweight.append(sumR_lightweight/divby)
    else:
      samples_ql.append(-r)
      samples_traditional.append(r_traditional)
      samples_lightweight.append(r_lightweight)

    if verbose:
      # if output_per_round is set, print output per request
      if show_averages:
        print(-sumR/divby, sumR_traditional/divby, sumR_lightweight/divby)
      else:
        print(-r, r_traditional, r_lightweight)

  if output_per_round:
    return samples_ql, samples_traditional, samples_lightweight
  else:
    return [-sumR/divby], [sumR_traditional/divby], [sumR_lightweight/divby]


