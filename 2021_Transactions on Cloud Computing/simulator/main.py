"""
To run the experiments, comment/uncomment the relevant section and execute: 
python main.py

[needs python3--better create a virtual environment and install required packages
from requirements.txt]
"""

import ql
import sys
import os
import psutil
import json
import copy
import numpy
from scipy.stats import sem, t, norm
from scipy import mean, median
import itertools
import math
import gc
import random
import itertools

# confidence intervals
confidence = 0.95

def setup_accept_probabilities(p):
  """ Setup specific (or random) accept probabilities for nodes of scenario p"""

  p["accept_probabilities"] = [1.0]*p["num_compute_nodes"]
  p["temp_accept_probabilities"] = [1.0]*p["num_compute_nodes"]

  for i in range(0, p["num_compute_nodes"]-1):
    init = 0.01
    temp = 0.5
    p["accept_probabilities"][i] = init
    p["temp_accept_probabilities"][i] = temp

  """  
  ######
  # Other options: select random node which will have a high prob to accept if activated
  p["accept_probabilities"] = [1.0]*p["num_compute_nodes"]
  p["temp_accept_probabilities"] = [1.0]*p["num_compute_nodes"]
  chosen = random.randint(0,p["num_compute_nodes"]-1)
  for i in range(0, p["num_compute_nodes"]-1):
    # random init accept probabilities in the range 0.01-0.1
    init = random.uniform(0.01, 0.1)
    if i == chosen:
      temp = random.uniform(0.8, 1.0)
      init = 0.5
    else:
      temp = 0.5
    p["accept_probabilities"][i] = init
    p["temp_accept_probabilities"][i] = temp
  ######
  """

if __name__ == "__main__":
  # Load scenario files with fog node topologies. Many parameters will be overriden later
  with open("scenarios/crosscontinent.json") as f:
    cross_continent = json.load(f)

  with open("scenarios/intracontinent.json") as f:
    intra_continent = json.load(f)

  with open("scenarios/smalllocaledge.json") as f:
    small_local_edge = json.load(f)

  # How much memory to dedicate to store the state (used to calculate k)
  ram_threshold = 2**28 # 256MiB

  # State representation: floating point numbers (Q-values) are represented using 4 bytes.
  bytes_per_element = 8 #1xfloat, 1x unint32
  ################

  """"
  # configuration
  # how many experiment iterations to run in order to get per-timestep rewards
  # set it to 1 in combination with averages=True to output cumulative average per time step
  loops = 1

  # cumulative averages or reward samples?
  averages = True
  
  # Experiment 1: Latency per round
  gamma = 0.90
  inactivity = 1
  epsilon = 1.0
  iterations = 1000
  # Run each scenario once and output all data in files w. 4 columns
  p = copy.deepcopy(cross_continent)

  # find a good k given e.g. a 1GB state size limitation s.t. n^(k+1)*4*2 <= state_size_threshold, 
  # assuming that the Q-table stores 4byte floating point numbers, and there are 2 values to maintain per pair (Q, action_counters)
  k = int(math.log(ram_threshold/bytes_per_element, p["num_compute_nodes"]) - 1)
  #k = 1
  print("Selected k: ", k)
  p["history_size"] = int(k)
  
  #lat_ql = [0]*p["iterations"]
  setup_accept_probabilities(p)
  p["inactivity_threshold"] = inactivity
  p["discount_factor"] = gamma
  p["epsilon"] = epsilon
  p["iterations"] = iterations
  lat_ucb = [0]*p["iterations"] #ignored
  print(p)

  arrs = []
  for l in range(0,loops):
    print("Loop ", l, end="\r")
    lat_ql, lat_trad, lat_simple = ql.loop(p, show_averages=averages, output_per_round=True, verbose=False)
    arr = numpy.array([lat_ql, lat_ucb, lat_trad, lat_simple]).T
    arrs.append(arr)
  print("")
  print(arr)
  numpy.savetxt("convergence_xcontinent_correct.dat", numpy.mean(arrs, axis=0), delimiter="\t", fmt="%.2f")
  # set show_averages to False and uncomment to output cumulative reward
  #numpy.savetxt("convergence_xcontinent.dat", numpy.cumsum(arr, axis=0), delimiter="\t", fmt="%.2f")
  
  p = copy.deepcopy(intra_continent)
  setup_accept_probabilities(p)
  p["inactivity_threshold"] = inactivity
  p["discount_factor"] = gamma
  p["epsilon"] = epsilon
  p["iterations"] = iterations
  print(p)

  k = int(math.log(ram_threshold/bytes_per_element, p["num_compute_nodes"]) - 1)
  #k = 1
  print("Selected k: ", k)
  p["history_size"] = int(k)
  lat_ql = [0]*p["iterations"]

  arrs = []
  for l in range(0,loops):
    print("Loop ", l, end="\r")
    lat_ql, lat_trad, lat_simple = ql.loop(p, show_averages=averages, output_per_round=True, verbose=False)
    arr = numpy.array([lat_ql, lat_ucb, lat_trad, lat_simple]).T
    arrs.append(arr)
  print("")
  numpy.savetxt("convergence_intracontinent_correct.dat", numpy.mean(arrs, axis=0), delimiter="\t", fmt="%.2f")
  # uncomment to output cumulative rewards  
  #numpy.savetxt("convergence_intracontinent.dat", numpy.cumsum(arr, axis=0), delimiter="\t", fmt="%.2f")

  p = copy.deepcopy(small_local_edge)
  setup_accept_probabilities(p)
  p["inactivity_threshold"] = inactivity
  p["discount_factor"] = gamma
  p["epsilon"] = epsilon
  p["iterations"] = iterations

  print(p)

  k = int(math.log(ram_threshold/bytes_per_element, p["num_compute_nodes"]) - 1)
  #k = 1
  print("Selected k: ", k)
  p["history_size"] = int(k)
  lat_ql = [0]*p["iterations"]

  arrs = []
  for l in range(0,loops):
    print("Loop ", l, end="\r")
    lat_ql, lat_trad, lat_simple = ql.loop(p, show_averages=averages, output_per_round=True, verbose=False)
    arr = numpy.array([lat_ql, lat_ucb, lat_trad, lat_simple]).T
    arrs.append(arr)
  print("")
  numpy.savetxt("convergence_small_correct.dat", numpy.mean(arrs, axis=0), delimiter="\t", fmt="%.2f")
  # uncomment to plot cumulative rewards
  # numpy.savetxt("convergence_small.dat", numpy.cumsum(arr, axis=0), delimiter="\t", fmt="%.2f")
  """

  ##################################
  
  # Experiment 2: Performance at round 100K for different acceptance probabilities
  # use gamma = 0.9, and start with alpha = 1.0, epsilon = 1.0 (they decay)
  iterations = 100000
  loops = 20
  inactivity = 1
  gamma = 0.9
  epsilon = 1.0

  aresults = []
  
  # Large-scale scenario
  # range of values for the acceptance probability
  R = itertools.chain([0.0001, 0.001, 0.005], numpy.arange(0.01, 0.1, 0.01), numpy.arange(0.1, 0.41, 0.1))
  for a in R:
    arr = []
    for j in range(0, loops):
      p = copy.deepcopy(cross_continent)
      p["inactivity_threshold"] = inactivity
      p["iterations"] = iterations
      p["history_size"] = int(math.log(ram_threshold/bytes_per_element, p["num_compute_nodes"]) - 1)
      p["discount_factor"] = gamma
      p["epsilon"] = epsilon

      # for each init accept probability in the range 0.01 - 0.5
      p["accept_probabilities"] = [1.0]*p["num_compute_nodes"]
      p["temp_accept_probabilities"] = [1.0]*p["num_compute_nodes"]
      for i in range(0, p["num_compute_nodes"]-1):
        # setup accept probabilities
        temp = 0.5
        p["accept_probabilities"][i] = a
        p["temp_accept_probabilities"][i] = temp

      lat_ql, lat_trad, lat_simple = ql.loop(p, show_averages=True, output_per_round=False, verbose=False)
      #print(lat_ql, lat_trad, lat_simple)
      arr.append([lat_ql[0], lat_trad[0], lat_simple[0]])
    arr = numpy.array(arr)
    m = mean(arr, axis=0)
    std_err = sem(arr, axis=0)
    ci = std_err * t.ppf((1 + confidence) / 2, len(arr) - 1)
    
    line = numpy.insert(mean(arr, axis=0), 0, a)
    #aresults.append(line)
    print(line)
    aresults.append([
      a, 
      m[0], m[0]-ci[0], m[0] + ci[0],
      m[1], m[1]-ci[1], m[1] + ci[1],
      m[2], m[2]-ci[2], m[2] + ci[2]
    ])
  #print(numpy.array(aresults))
  numpy.savetxt("cross-fas-ci.dat", numpy.array(aresults), delimiter="\t", fmt="%.4f")
  
  # Medium-scale scenario
  aresults = []
  R = itertools.chain([0.0001, 0.001, 0.005], numpy.arange(0.01, 0.1, 0.01), numpy.arange(0.1, 0.41, 0.1))
  for a in R:
    arr = []
    for j in range(0, loops):
      p = copy.deepcopy(intra_continent)
      p["inactivity_threshold"] = inactivity
      p["iterations"] = iterations
      p["history_size"] = int(math.log(ram_threshold/bytes_per_element, p["num_compute_nodes"]) - 1)
      p["discount_factor"] = gamma
      p["epsilon"] = epsilon

      # for each init accept probability in the range 0.01 - 0.5
      p["accept_probabilities"] = [1.0]*p["num_compute_nodes"]
      p["temp_accept_probabilities"] = [1.0]*p["num_compute_nodes"]
      for i in range(0, p["num_compute_nodes"]-1):
        # setup accept probabilities
        temp = 0.5
        p["accept_probabilities"][i] = a
        p["temp_accept_probabilities"][i] = temp

      lat_ql, lat_trad, lat_simple = ql.loop(p, show_averages=True, output_per_round=False, verbose=False)
      #print(lat_ql, lat_trad, lat_simple)
      arr.append([lat_ql[0], lat_trad[0], lat_simple[0]])
    arr = numpy.array(arr)
    m = mean(arr, axis=0)
    std_err = sem(arr, axis=0)
    ci = std_err * t.ppf((1 + confidence) / 2, len(arr) - 1)
    
    line = numpy.insert(mean(arr, axis=0), 0, a)
    #aresults.append(line)
    print(line)
    aresults.append([
      a, 
      m[0], m[0]-ci[0], m[0] + ci[0],
      m[1], m[1]-ci[1], m[1] + ci[1],
      m[2], m[2]-ci[2], m[2] + ci[2]
    ])
    
  #print(numpy.array(aresults))
  numpy.savetxt("intra-fas-ci.dat", numpy.array(aresults), delimiter="\t", fmt="%.4f")

  # small-scale scenario
  aresults = []
  R = itertools.chain([0.0001, 0.001, 0.005], numpy.arange(0.01, 0.1, 0.01), numpy.arange(0.1, 0.41, 0.1))
  for a in R:
    arr = []
    for j in range(0, loops):
      p = copy.deepcopy(small_local_edge)
      p["inactivity_threshold"] = inactivity
      p["iterations"] = iterations
      p["history_size"] = int(math.log(ram_threshold/bytes_per_element, p["num_compute_nodes"]) - 1)
      p["discount_factor"] = gamma
      p["epsilon"] = epsilon

      # for each init accept probability in the range 0.01 - 0.5
      p["accept_probabilities"] = [1.0]*p["num_compute_nodes"]
      p["temp_accept_probabilities"] = [1.0]*p["num_compute_nodes"]
      for i in range(0, p["num_compute_nodes"]-1):  
        # setup accept probabilities
        temp = 0.5
        p["accept_probabilities"][i] = a
        p["temp_accept_probabilities"][i] = temp

      lat_ql, lat_trad, lat_simple = ql.loop(p, show_averages=True, output_per_round=False, verbose=False)
      #print(lat_ql, lat_trad, lat_simple)
      arr.append([lat_ql[0], lat_trad[0], lat_simple[0]])
    arr = numpy.array(arr)
    m = mean(arr, axis=0)
    std_err = sem(arr, axis=0)
    ci = std_err * t.ppf((1 + confidence) / 2, len(arr) - 1)
    
    line = numpy.insert(mean(arr, axis=0), 0, a)
    #aresults.append(line)
    print(line)
    aresults.append([
      a, 
      m[0], m[0]-ci[0], m[0] + ci[0],
      m[1], m[1]-ci[1], m[1] + ci[1],
      m[2], m[2]-ci[2], m[2] + ci[2]
    ])
    
  #print(numpy.array(aresults))
  numpy.savetxt("small-fas-ci.dat", numpy.array(aresults), delimiter="\t", fmt="%.4f")

