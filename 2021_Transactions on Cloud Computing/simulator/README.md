# Q-learning-based request routing strategy

## Intro
Here is the implementation of a Q-learning algorithm for application request routing (CA-QL), which can be simulated on a variety of scenarios (see simulator/params for 3 characteristic ones). We are also simulating a traditional request routing logic, as well as a simple context-aware one that however does not use learning.

## Contents
- `results`: Results of our simulations for 3 different deployment scenarios, as described in our article. Each file includes 10 columns: The 1st corresponds to the initial/default acceptance probability of a compute node (x axis), and the next three 3-tuples are the mean value and the 95% confidence interval bounds for the CA-QL, traditional and the simple context-aware mechanisms, respectively. Gnuplot scripts are also provided to visualize our results. 
- `scenarios`: JSON files which describe specific compute node topologies, scenario settings, and related parameters for the Q-learning algorithm.
- `ql.py`: Q-learning algorithm implementation.
- `main.py`: Python script that executes a set of simulation experiments.
- `requirements.txt`: Required python libraries.

## Run
The most straightforward way to run this code is to create a Python 3 virtual environment and install the dependencies there (e.g., `pip install -r requirements.txt` inside the virtual environment). To re-run the experiments, just execute `python main.py`.

