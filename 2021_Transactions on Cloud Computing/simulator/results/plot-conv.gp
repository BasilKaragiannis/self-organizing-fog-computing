# Run it like this: gnuplot -e "datafile='/path/to/data/file'" -e "outfile='/path/to/output/file'" plot.gp

set terminal postscript eps enhanced color font 'Times,24'
set output outfile

set xlabel font "Times,32"
set ylabel font "Times,32"
set xtics font "Times,32"
set ytics font "Times,32"

set xlabel "Time step (number of requests)"
set ylabel "Mean latency (ms)"
set yrange [0:400]
#set xrange[0:1000]
set xtics 0,1000,5000
#unset xtics

plot \
  datafile u 1 w l t "CA-QL",\
  datafile u 3 w l t "Traditional", \
  datafile u 4 w l t "CA-NL"

