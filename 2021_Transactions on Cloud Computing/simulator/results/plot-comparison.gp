set terminal postscript eps enhanced color font 'Times,24'
set output "x-fas.eps"

set xlabel font "Times,32"
set ylabel font "Times,32"
set xtics font "Times,32"
set ytics font "Times,32"

#set title "Cross-continent deployment"
set xlabel "Initial acceptance probability"
set ylabel "Mean latency (ms)"
set yrange [0:400]
plot \
  "cross-fas-ci.dat" u 1:5:6:7 w errorlines t "Traditional",\
  "cross-fas-ci.dat" u 1:8:9:10 w errorlines t "Context-aware, no learning",\
  "cross-fas-ci.dat" u 1:2:3:4 w errorlines t "Context-aware, Q-Learning"


#########################
set output "i-fas.eps"

set xlabel font "Times,32"
set ylabel font "Times,32"
set xtics font "Times,32"
set ytics font "Times,32"

#set title "Intra-continent deployment"
set xlabel "Initial acceptance probability"
set ylabel "Mean latency (ms)"

plot \
  "intra-fas-ci.dat" u 1:5:6:7 w errorlines t "Traditional",\
  "intra-fas-ci.dat" u 1:8:9:10 w errorlines t "Context-aware, no learning",\
  "intra-fas-ci.dat" u 1:2:3:4 w errorlines t "Context-aware, Q-Learning"

#########################

set output "s-fas.eps"

set xlabel font "Times,32"
set ylabel font "Times,32"
set xtics font "Times,32"
set ytics font "Times,32"

#set title "Small-scale deployment (1 edge node, 1 cloud node)"
set xlabel "Initial acceptance probability"
set ylabel "Mean latency (ms)"

plot \
  "small-fas-ci.dat" u 1:5:6:7 w errorlines t "Traditional",\
  "small-fas-ci.dat" u 1:8:9:10 w errorlines t "Context-aware, no learning",\
  "small-fas-ci.dat" u 1:2:3:4 w errorlines t "Context-aware, Q-Learning"

