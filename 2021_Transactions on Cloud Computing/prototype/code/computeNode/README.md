The folder "source" contains the source code of a web server that imprements an API to either accept application requests or to forward them to the next node on path.
The executable file "computeNode8001.jar" is the packaged version of the webserver that runs on each compute node of the system. By default, the server run on port 8001.
