This folder contains the source code and executable files used in the evaluation.
The folder "computeNode" contains the files related to the software that runs on each compute node of the system.
The folder "gateway" contains the files related to the software that runs on the gateway.