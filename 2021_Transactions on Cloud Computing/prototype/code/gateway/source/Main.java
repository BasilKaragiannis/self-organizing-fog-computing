package gateway;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

public class Main {

	public static void main(String[] args) {
		String[] nodes={				
				/*				"34.65.83.80:8001",//first node is the node in proximity lowest acceptance rate
				"localhost:8002",
				"localhost:8003",
				"localhost:8004",
				"localhost:8005",
				"localhost:8006",
				"localhost:8007",
				"localhost:8008",
				"localhost:8009",
				"localhost:8010"};/*
				"localhost:8011",
				"localhost:8012"};//n12 taiwan //*/

				//*				//first node is the node in proximity lowest acceptance rate
				"34.65.83.80:8001",		//zurich
				"35.198.88.63:8001",	//frankfurt
				"34.76.126.157:8001",	//belgium
				"34.90.117.192:8001",	//netherlands
				"34.105.175.90:8001",	//london
				"35.203.36.25:8001",	//montreal
				"34.86.169.133:8001",	//virginia
				"34.72.126.121:8001",	//iowa
				"34.106.147.209:8001",	//salt lake city
				"34.94.229.117:8001"};	//los angeles
		//*/

		//find hops for each node
		/*
		int[] hopsTable=new int[nodes.length];
		for (int i=0;i<nodes.length;i++) {
			hopsTable[i]=Utils.findHops(nodes[i]);
			System.out.println(hopsTable[i]);
		}//*/
		//hop count is always the same so to save time it is hardcoded
		int[] hopsTable= {15,18,22,23,18,21,23,22,25,26};

		for (int i=0;i<nodes.length;i++) {
			Utils.sendGetRequest("http://"+nodes[i]+"/resetGroups");
		}

		/*
		try{
			//picture of 350KB
			message = new String ( Files.readAllBytes( Paths.get("./picture.pgm.txt") ) );
		}catch (IOException e){
			e.printStackTrace();
		}//*/
		//for every file size
		/*
		for (int m=0;m<2;m++) {
			System.out.println("----------------"+m+"----------------");
			consoleOutput=consoleOutput+"----------------"+m+"----------------"+"\n";
			if(m==1) {//1050KB
				message=message+message+message;
			}else {//2100KB
				message=message+message;
			}*/

		Utils.sendGetRequest("http://"+nodes[0]+"/putHostNodeIP?hostNodeIP="+nodes[0]);
		//		Utils.sendGetRequest("http://"+nodes[0]+"/putAcceptanceRate?acceptanceRate="+10);
		//putHostNodeIP
		for (int i=1;i<nodes.length;i++) {
			Utils.sendGetRequest("http://"+nodes[i]+"/putHostNodeIP?hostNodeIP="+nodes[i]);
			//			Utils.sendGetRequest("http://"+nodes[i]+"/putAcceptanceRate?acceptanceRate="+((i*10)+10));
			Utils.sendGetRequest("http://"+nodes[i]+"/putContactNode?contactNode="+nodes[i-1]);
		}
		String consoleOutput="";
		
		Utils.sendGetRequest("http://"+nodes[0]+"/putAcceptanceRate?acceptanceRate="+7);
		Utils.sendGetRequest("http://"+nodes[1]+"/putAcceptanceRate?acceptanceRate="+7);
		Utils.sendGetRequest("http://"+nodes[2]+"/putAcceptanceRate?acceptanceRate="+7);
		Utils.sendGetRequest("http://"+nodes[3]+"/putAcceptanceRate?acceptanceRate="+7);
		Utils.sendGetRequest("http://"+nodes[4]+"/putAcceptanceRate?acceptanceRate="+7);
		Utils.sendGetRequest("http://"+nodes[5]+"/putAcceptanceRate?acceptanceRate="+7);
		Utils.sendGetRequest("http://"+nodes[6]+"/putAcceptanceRate?acceptanceRate="+7);
		Utils.sendGetRequest("http://"+nodes[7]+"/putAcceptanceRate?acceptanceRate="+7);
		Utils.sendGetRequest("http://"+nodes[8]+"/putAcceptanceRate?acceptanceRate="+7);
		Utils.sendGetRequest("http://"+nodes[9]+"/putAcceptanceRate?acceptanceRate="+100);

		String message="0";
		int experiments=200;
		String time=String.valueOf(new Date().getTime());
		int latencySum=0;
		for(int j=0;j<2;j=j+2) {			

			System.out.println("------------"+j+"------------");
			consoleOutput=consoleOutput+"------------"+j+"------------"+"\n";
			String latencyCountLines="";
			String hopCountLines="";
			String previouslyUtilizedNode="";
			String firstNode="";
			int hops=0;

			for (int i=0;i<experiments;i++) {

				if(j==0) {
					firstNode=nodes[0];
				}else if(i%j==0) {
					firstNode=nodes[0];					
				}

				//find hops to the first node
				for(int n=0;n<nodes.length;n++) {
					if(nodes[n].equals(firstNode)) {
						hops=hopsTable[n];
						break;
					}
				}
				try {
					Thread.sleep(200);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//post message
				String targetUrl="http://"+firstNode+"/postMessage?"
						+ "originalSender=&"
						+ "hopCount="+hops+"&"
						+ "latencyCount=0"+"&"
						+ "previousArrivalDate="+(new Date().getTime());
				String response=Utils.postRequest(targetUrl, message);
				System.out.println(response);
				consoleOutput=consoleOutput+response+"\n";

				hopCountLines=hopCountLines+response.split(" ")[0]+"\n";
				latencyCountLines=latencyCountLines+response.split(" ")[1]+"\n";
				latencySum=latencySum+Integer.valueOf(response.split(" ")[1]);
				previouslyUtilizedNode=response.split(" ")[2];
				firstNode=previouslyUtilizedNode;
				//set the probabilities
//*				
				Utils.sendGetRequest("http://"+nodes[0]+"/putAcceptanceRate?acceptanceRate="+7);
				Utils.sendGetRequest("http://"+nodes[1]+"/putAcceptanceRate?acceptanceRate="+7);
				Utils.sendGetRequest("http://"+nodes[2]+"/putAcceptanceRate?acceptanceRate="+7);
				Utils.sendGetRequest("http://"+nodes[3]+"/putAcceptanceRate?acceptanceRate="+7);
				Utils.sendGetRequest("http://"+nodes[4]+"/putAcceptanceRate?acceptanceRate="+7);
				Utils.sendGetRequest("http://"+nodes[5]+"/putAcceptanceRate?acceptanceRate="+7);
				Utils.sendGetRequest("http://"+nodes[6]+"/putAcceptanceRate?acceptanceRate="+7);
				Utils.sendGetRequest("http://"+nodes[7]+"/putAcceptanceRate?acceptanceRate="+7);
				Utils.sendGetRequest("http://"+nodes[8]+"/putAcceptanceRate?acceptanceRate="+7);
//				Utils.sendGetRequest("http://"+nodes[9]+"/putAcceptanceRate?acceptanceRate="+100);
				//set the probability of the previouslyUtilizedNode
				if(!firstNode.equals(nodes[9])){
					Utils.sendGetRequest("http://"+firstNode+"/putAcceptanceRate?acceptanceRate="+80);
				}
//*/
				try {
					BufferedWriter writer = new BufferedWriter(new FileWriter("./data/"+
							j+"MB_"+j+"reset"+"_hopCount.txt"));
					writer.write(hopCountLines);
					writer.close();
					BufferedWriter writer2 = new BufferedWriter(new FileWriter("./data/"+
							j+"MB_"+j+"reset"+"_latencyCount.txt"));
					writer2.write(latencyCountLines);
					writer2.close();
					BufferedWriter writer3 = new BufferedWriter(new FileWriter("./data/"+
							"consoleOutput_"+time+".txt"));
					writer3.write(consoleOutput);
					writer3.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}
			System.out.println("avg= "+latencySum/experiments);
			consoleOutput=consoleOutput+"avg= "+latencySum/experiments+"\n";
			latencySum=0;
			try {
				BufferedWriter writer3 = new BufferedWriter(new FileWriter("./data/"+
						"consoleOutput_"+time+".txt"));
				writer3.write(consoleOutput);
				writer3.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

