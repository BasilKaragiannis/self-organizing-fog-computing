<PhotovoltaicArray startDateTime="2013-10-01T00:00:00Z" id="PhotovoltaicArray1" ratingWp="3500"/>


-<Meter startDateTime="2013-10-01T00:00:00Z" id="Meter1" meterType="Gas">


-<Sensor startDateTime="2013-09-14T00:00:00Z" id="Sensor1228" endDateTime="2015-05-06T23:30:00Z" model="Replacement gas meter" manufacturer="Smart Metering Solutions">

<TimeSeriesVariable startDateTime="2013-09-14T00:00:00Z" id="TimeSeriesVariable1554" endDateTime="2015-05-06T23:30:00Z" hasDuplicateTimestamps="No" repeatsOmitted="No" hasMissingData="Yes" intervalLength="30" intervalUnit="Minute" intervalType="FixedInterval" units="Cubic metres" variableType="Gas volume"/>

</Sensor>

</Meter>