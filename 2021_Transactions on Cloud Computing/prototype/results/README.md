This folder contains various files related to the results of our implementation.
The file "gasMeter.xml.txt" contains the XML description of the gas sensor that produced the gas measurements used as input data in our experiments.
The file "gasValues.csv" contains the input data.
The file "overallResults_proposedRouting.xlsx" contains the results from reaching each compute node of the system based on the proposed routing, as well as the percentages of reduction compared to the traditional routing.
The file "overallResults_traditionalRouting.xlsx" contains the results from reaching each compute node of the system based on the taditional routing.
The file "specifiedScenarioData.xlsx" contains results which were acquired during the runtime of a specified scenario with emulated dynamic load. These results include measurements from both the traditional routing and the proposed routing, the precentages of reduction, and the figures.
The file "specifiedScenarioData_additionalResults.xlsx" contains additional results which are related to different precentages of acceptance by compute nodes on path.
The file "trendlines.xlsx" contains the functions and coeffcients of determination of the trendlines.