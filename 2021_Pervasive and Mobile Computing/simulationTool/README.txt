This folder includes the source code of a tool that can be used for simulating fog computing structures. It can be very useful for conducting experiments with a very large number of nodes (e.g., thousands).

The folder "hierarchical" contains the source code that can be used for simulating hierarchical structures (i.e., HPA and HPI). The folder "flat" contains the source code that can be used for simulating flat structures (i.e., FPA and FPI). For OPT, FPA with a group size that equals the number of the nodes in the system can be used. 

To run the simulation, first modify the \src\global\Global file, and change the system parameters to the desired values. These system parameters can be, e.g., the number of nodes in the system, the maximum group size, the number of iterations, the number of nodes that fail, etc. Afterwards, simply run the "Main.java" file, and the simulator will create text files with the results.

The generated results of the overhead have the number of control messages for each new node, for exmaple, each line has values separated by space, e.g.:
0 7.92 12.52 18.48 48.14 ...
This means that the 1st node causes zero messages. The 2nd node causes (by average of the number of iterations) 7.92 messages. The 3rd node causes 12.52 messages, the 4th causes 18.48 messages, etc. These messages refer to control messages that are used for self-organization, not application messages. This is why these messages are considered as overhead.