Within the folder "rawData" you may find all the results of the experiments regarding latency. The file names (inside the rawData folder) have 5 fields separated by "-", e.g.:
1-FPA-allLatencies-1-5 16 6 3 14 1 9 11 17 18 12 15 8 4 7 13 10 2
1st field: the file size (1 means 1.2Mb, 2 means 2.4Mb, 4 means 4.8Mb).
2st field: organization type (HPA, HPI, FPA, FPI, OPT as defined in the paper).
3nd field: denotes that the files contains the latencies among all the other (17) nodes.
4rd field: enumeration 1-50 due to 50 experiments.
5th field: order that the nodes join the system; each node has an identifier 1-18.
Within each file (inside the rawData folder) you may find 15 lines, one for each one of the 15 low-end nodes. Each line has 17 fields (ordered) which show the latency towards each one of the other 17 nodes.

There are also 3 "comparizons.xlsx" files. These are based on results from the "rawData" folder. Each one starts with a number indicating the file size (1 means 1.2Mb, 2 means 2.4Mb, 4 means 4.8Mb). Within "4-comparisons.xlsx", the three lowest latencies of each node, are grouped together in order to plot the latency towards the three closest neighbors for each node. Similarly, the rest of the latencies of each node (i.e., excluding the three closest neighbors) are also grouped together and plotted to show the latencies towards farther nodes. There are also the average values and the standard deviation. Within "1-comparisons.xlsx" and "2-comparisons.xlsx", there are the average values and standard deviation of the other file sizes.