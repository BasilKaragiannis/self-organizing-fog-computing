import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

public class Utils {
	/**sends a GET request to targetUrl
	 * @param targetUrl the address of the get request
	 * @return the response of the get request
	 */
	public static String sendGetRequest(String targetUrl) {
		StringBuffer response = new StringBuffer();
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL(targetUrl).
					openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Type", "x-www-form-urlencoded");
			connection.setConnectTimeout(600000);
			if (connection.getResponseCode() != 200) {
				throw new RuntimeException("GET Request to "+ targetUrl +
						" failed with Error code : "
						+ connection.getResponseCode());
			}
			BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
					(connection.getInputStream())));
			String output;
			while((output=responseBuffer.readLine()) != null){
				response.append(output+"\n");
			}   
			connection.disconnect();
		}catch (Exception e) {
			System.out.println(e.getMessage());
			return "connectionTimeout";
		}
		return response.toString();
	}
	
	/**sends a POST request to targetUrl
	 * @param targetUrl the address of the POST request
	 * @param input a string to be sent through the POST request
	 * @return the HTTP code the response of the POST request
	 */
	public static int postRequest(String targetUrl, String body) {
		int responseCode=0;
		try {
			URL url = new URL(targetUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(60000);
			connection.setReadTimeout(60000);
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "text/plain");

			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());  
			out.write(body);
			out.flush();
			out.close();

			responseCode = connection.getResponseCode();
			InputStream is = connection.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while((line = br.readLine() ) != null) {
				//  System.out.println(line);
			}

			connection.disconnect();
		}catch(Exception e){ // TODO: catch the thrown Exception! NEVER catch "Exception"
			// TODO: log instead of print
			System.out.println(e.getMessage());
		}
		return responseCode;
	}
	
	public static String[] generateJoinOrder(Random random, ArrayList<String> cloudNodes,
			int numOfNodes, int instances) {
		String[] joinOrder=new String[instances];
		int lineCounter=0;
		for (int i=0;i<instances;i++) {
			ArrayList<String> nodes=new ArrayList<String>();
			for(int n=1;n<numOfNodes;n++) {
				nodes.add(String.valueOf(n));
			}
			while(true) {
				String firstNode=String.valueOf(random.nextInt(numOfNodes-1)+1);
				if(cloudNodes.contains(String.valueOf(firstNode))){
					joinOrder[lineCounter]=firstNode+" ";
					nodes.remove(firstNode);
					break;
				}
			}
			while(nodes.size()>0) {
				String randomNode=String.valueOf(random.nextInt(numOfNodes-1)+1);
				if(nodes.contains(String.valueOf(randomNode))){
					joinOrder[lineCounter]=joinOrder[lineCounter]+randomNode+" ";
					nodes.remove(randomNode);
				}
			}
			lineCounter++;
		}
		return joinOrder;
	}
	
	public static double findMin(ArrayList<Double> list) {
		double min=Double.MAX_VALUE;
		for(double value:list) {
			if(value<min) {
				min=value;
			}
		}
		return min;
	}
	
	public static double findMax(ArrayList<Double> list) {
		double max=0;
		for(double value:list) {
			if(value>max) {
				max=value;
			}
		}
		return max;
	}
	
	public static String[] sortList(ArrayList<Double> list) {
		String[]sortedTable=new String[list.size()];
		
		for(int i=0;i<sortedTable.length;i++) {
			double min=Utils.findMin(list);
			sortedTable[i]=String.valueOf(min);
			list.remove(min);
		}
		return sortedTable;
	}


}
