import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		String allLatencies=""; //sorted
		String minLatencies=""; //3 smaller 
		String maxLatencies=""; //all apart from 3 smaller
		String[] organizationType= {"FPI", "HPI", "FPA", "HPA"}; //"HPA", "HPI", "FPA", "FPI"
		//global random with seed
		Random random=new Random(0);
		//define the cloud nodes
		ArrayList<String> cloudNodes=new ArrayList<String>();
		cloudNodes.add("1");
		cloudNodes.add("5");
		cloudNodes.add("9");

		//table of nodes with particular order n1...n12
		String[] nodes={"",
/*
				"localhost:8001",
				"localhost:8002",
				"localhost:8003",
				"localhost:8004",
				"localhost:8005",
				"localhost:8006",
				"localhost:8007",
				"localhost:8008",
				"localhost:8009",
				"localhost:8010",
				"localhost:8011",
				"localhost:8012"};//n12 taiwan //*/

//*				
				"35.246.138.200:8001",//n1 frankfurt
				"128.131.172.181:8001",//"128.131.172.181:8001",//n2 vienna
				"128.131.172.182:8001",//"128.131.172.182:8001",//n3 vienna
				"34.65.65.65:8001",//n4 zurich
				"35.194.70.158:8001",//n5 N virginia
				"35.227.43.216:8001",//n6 S carolina
				"35.203.1.205:8001",//n7 montreal
				"34.95.55.207:8001",//n8 montreal
				"34.97.126.222:8001",//n9 osaka
				"34.84.50.205:8001",//n10 tokyo
				"34.85.64.32:8001",//n11 tokyo
				"34.80.251.249:8001",//n12 taiwan  //*/
		
				"35.242.236.201:8001",//n13 frankfurt
				"34.65.59.23:8001",//n14 zurich
				"35.230.172.121:8001",//n15 N virginia
				"35.237.0.40:8001",//n16 N carolina
				"34.97.61.189:8001",//n17 osaka
				"104.199.244.30:8001"};//n18 taiwan

		//table of join order
		int instances=50;
		int numOfNodes=19; //number of nodes +1
		String[] joinOrder=Utils.generateJoinOrder(random, cloudNodes, numOfNodes, instances);
		//putHostNodeIP
		for (int i=1;i<nodes.length;i++) {
			Utils.sendGetRequest("http://"+nodes[i]+"/putHostNodeIP?hostNodeIP="+nodes[i]);
		}

		for(int org=0;org<organizationType.length;org++) {
			//set the organization type in the nodes
			for (int i=1;i<nodes.length;i++) {
				Utils.sendGetRequest("http://"+nodes[i]+
						"/putOrganizationType?OrganizationType="+organizationType[org]);
			}
System.out.println(organizationType[org]);
			//for every join order
			for (int i=0;i<joinOrder.length;i++) {
System.out.println(joinOrder[i]);
				//reset nodes
				for (int n=1;n<nodes.length;n++) {
					Utils.sendGetRequest("http://"+nodes[n]+"/resetGroups");
				}
				//get the first join order
				String[] line=joinOrder[i].split(" ");
				//put root node. in flat
				if(organizationType[org].contains("H")) {
					Utils.sendGetRequest("http://"+nodes[Integer.valueOf(line[0])]+"/putRootNode");
				}
				//contact is the first node of the order
				String contactNode=nodes[Integer.valueOf(line[0])];
				//add each new node
				for(int j=1;j<line.length;j++) {
					//find random contact
					if(organizationType[org].contains("F")) {
						contactNode=nodes[Integer.valueOf(line[random.nextInt(j)])];
						//find index of contactNode
						int contactNodeIndex=0;
						for(int k=1;k<nodes.length;k++) {
							if(contactNode.equals(nodes[k])) {
								contactNodeIndex=k;
								break;
							}
						}
						while (!cloudNodes.contains(String.valueOf(contactNodeIndex))) {
							contactNode=nodes[Integer.valueOf(line[random.nextInt(j)])];
							//find index of contactNode
							for(int k=1;k<nodes.length;k++) {
								if(contactNode.equals(nodes[k])) {
									contactNodeIndex=k;
									break;
								}
							}
						}
					}
					//join
					Utils.sendGetRequest("http://"+nodes[Integer.valueOf(line[j])]+
							"/putContactNode?contactNode="+contactNode);
System.out.println(line[j]);
/*					try {//wait for all traceroutes
						if(organizationType[org].equals("HPA")) {
							Thread.sleep(5000);
						}else if(organizationType[org].equals("HPI")) {
							Thread.sleep(10000);//60
						}else if(organizationType[org].equals("FPA")) {
							Thread.sleep(5000);
						}else if(organizationType[org].equals("FPI")) {
							Thread.sleep(10000);//60
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
				}
				//load message to string
				String message="";
				try{
					message = new String ( Files.readAllBytes( Paths.get("./picture.txt") ) );
				}catch (IOException e){
					e.printStackTrace();
				}
				//make a 4.8 Mb file. each message is 1.2Mb
				
				
				for(int m=1;m<3;m++){
				if(m==2) {
					message=message+message;
				}
				
//				output=output+"-----"+joinOrder[i]+"-----"+"\n";
				for(int k=1;k<nodes.length;k++) {
					if(!cloudNodes.contains(String.valueOf(k))) {
						//reset message arrival time
						for (int n=1;n<nodes.length;n++) {
							Utils.sendGetRequest("http://"+nodes[n]+"/resetMessage");
						}
						//post message
						String targetUrl="http://"+nodes[k]+"/postMessage?originalSender=";
						Utils.postRequest(targetUrl, message);
						//wait for messages to arrive
/*						try {
							Thread.sleep(15000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}*/
//						output=output+"sender: "+k+"\n";
						ArrayList<Double> latencyList=new ArrayList<Double>();
						double senderArrivalTime=Double.valueOf(Utils.sendGetRequest(
								"http://"+nodes[k]+"/getMessageArrivalTime"));
						for(int l=1;l<nodes.length;l++) {
							double receiverArrivalTime=Double.valueOf(Utils.sendGetRequest(
									"http://"+nodes[l]+"/getMessageArrivalTime"));
							double latency=receiverArrivalTime-senderArrivalTime;
							if(latency!=0) {
								latencyList.add(latency);
							}
						}
						//empties list due to pass by reference
						String[] sortedLatencyTable=Utils.sortList(latencyList);

						for (int l=0;l<sortedLatencyTable.length;l++) {
							allLatencies=allLatencies+sortedLatencyTable[l]+" ";
							if(l<3) {
								minLatencies=minLatencies+sortedLatencyTable[l]+" ";
							}else {
								maxLatencies=maxLatencies+sortedLatencyTable[l]+" ";
							}
						}
						allLatencies=allLatencies+"\n";
					}
				}
				allLatencies=allLatencies+"\n";
				//for every joinOrder
				//write to file
				try {
					BufferedWriter writer = new BufferedWriter(new FileWriter(
							m+"-"+organizationType[org]+"-allLatencies-"+(i+1)+"-"+joinOrder[i]+".txt"));
					writer.write(allLatencies);
					writer.close();
					BufferedWriter writer2 = new BufferedWriter(new FileWriter(
							m+"-"+organizationType[org]+"-minLatencies-"+(i+1)+"-"+joinOrder[i]+".txt"));
					writer2.write(minLatencies);
					writer2.close();
					BufferedWriter writer3 = new BufferedWriter(new FileWriter(
							m+"-"+organizationType[org]+"-maxLatencies-"+(i+1)+"-"+joinOrder[i]+".txt"));
					writer3.write(maxLatencies);
					writer3.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				allLatencies="";
				minLatencies="";
				maxLatencies="";
				}
				
//for debugging				break;/////////////////////////////////////////////////////////////////////////////////
				
				
			}
		}
//for optimal		
//*		
		for (int i=0;i<joinOrder.length;i++) {
		
		//optimal topology statically configured with FPA
		//set the organization type in the nodes to FPA
		for (int n=1;n<nodes.length;n++) {
			Utils.sendGetRequest("http://"+nodes[n]+"/putOrganizationType?OrganizationType="+"FPA");
			Utils.sendGetRequest("http://"+nodes[n]+"/putGroupSize?groupSize=20");
		}
		//for this join order
System.out.println(joinOrder[i]);
		//reset nodes
		for (int n=1;n<nodes.length;n++) {
			Utils.sendGetRequest("http://"+nodes[n]+"/resetGroups");
		}
		//get the first join order
		String[] line=joinOrder[i].split(" ");
		//contact is the first node of the order

		String contactNode=nodes[Integer.valueOf(line[0])];
		//add each new node
		for(int j=1;j<line.length;j++) {
			//change contact
			if(j>6) {contactNode=nodes[Integer.valueOf(line[1])];}
			if(j>9) {contactNode=nodes[Integer.valueOf(line[2])];}
			if(j>11) {contactNode=nodes[Integer.valueOf(line[0])];}
			if(j>13) {contactNode=nodes[Integer.valueOf(line[1])];}
			if(j>15) {contactNode=nodes[Integer.valueOf(line[2])];}
			//join
			Utils.sendGetRequest("http://"+nodes[Integer.valueOf(line[j])]+
					"/putContactNode?contactNode="+contactNode);
System.out.println(line[j]);
//			try {//wait for all traceroutes
//				Thread.sleep(5000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
		//load message to string
		String message="";
		try{
			message = new String ( Files.readAllBytes( Paths.get("./picture.txt") ) );
		}catch (IOException e){
			e.printStackTrace();
		}
		
		for(int m=1;m<3;m++){
		if(m==2) {
			message=message+message;
		}
		
//		output=output+"-----"+optimalJoinOrder+"-----"+"\n";
		for(int k=1;k<nodes.length;k++) {
			if(!cloudNodes.contains(String.valueOf(k))) {
System.out.println("sender: "+k);
				//reset message arrival time
				for (int n=1;n<nodes.length;n++) {
					Utils.sendGetRequest("http://"+nodes[n]+"/resetMessage");
				}
				//post message
				String targetUrl="http://"+nodes[k]+"/postMessage?originalSender=";
				Utils.postRequest(targetUrl, message);
				//wait for messages to arrive
//				try {
//					Thread.sleep(10000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				allLatenciesMinMax=allLatenciesMinMax+"sender: "+k+"\n";
				ArrayList<Double> latencyList=new ArrayList<Double>();
				double senderArrivalTime=Double.valueOf(Utils.sendGetRequest(
						"http://"+nodes[k]+"/getMessageArrivalTime"));
				for(int l=1;l<nodes.length;l++) {
					double receiverArrivalTime=Double.valueOf(Utils.sendGetRequest(
							"http://"+nodes[l]+"/getMessageArrivalTime"));
					double latency=receiverArrivalTime-senderArrivalTime;
					if(latency!=0) {
						latencyList.add(latency);
					}
				}
				//empties list due to pass by reference
				String[] sortedLatencyTable=Utils.sortList(latencyList);

				for (int l=0;l<sortedLatencyTable.length;l++) {
					allLatencies=allLatencies+sortedLatencyTable[l]+" ";
					if(l<3) {
						minLatencies=minLatencies+sortedLatencyTable[l]+" ";
					}else {
						maxLatencies=maxLatencies+sortedLatencyTable[l]+" ";
					}
				}
				allLatencies=allLatencies+"\n";
			}
		}
		allLatencies=allLatencies+"\n";
		//for every joinOrder
		//write to file
		try  {
			BufferedWriter writer = new BufferedWriter(new FileWriter(
					m+"-"+"optimalFPA"+"-allLatencies-"+(i+1)+"-"+joinOrder[i]+".txt"));
			writer.write(allLatencies);
			writer.close();
			BufferedWriter writer2 = new BufferedWriter(new FileWriter(
					m+"-"+"optimalFPA"+"-minLatencies-"+(i+1)+"-"+joinOrder[i]+".txt"));
			writer2.write(minLatencies);
			writer2.close();
			BufferedWriter writer3 = new BufferedWriter(new FileWriter(
					m+"-"+"optimalFPA"+"-maxLatencies-"+(i+1)+"-"+joinOrder[i]+".txt"));
			writer3.write(maxLatencies);
			writer3.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		allLatencies="";
		minLatencies="";
		maxLatencies="";
		//*/
		}
		}
	}
}
