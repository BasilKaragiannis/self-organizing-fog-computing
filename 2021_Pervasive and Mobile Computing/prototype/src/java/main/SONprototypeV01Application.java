package main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SONprototypeV01Application {

	public static void main(String[] args) {
		SpringApplication.run(SONprototypeV01Application.class, args);
	}
}
