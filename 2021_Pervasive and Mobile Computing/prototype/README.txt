The file "prototype8001_v01.jar" is a prototype implementation that can be used for building fog computing systems consisting of multiple geographically distributed compute nodes. Follow the tutorial found in the home page to discover its functionality.

The folder "src" includes the source code of the prototype. Among the classes, the class \src\java\globalVariables contains system parameters, the class \src\java\main\WebController contains the utilized API, and the file \src\resources\application contains the port that the application will run on (8001 by default).

The folder "lib" includes the jGraphT library which is used to create graphs representing groups of nodes with hop count as weights.

The folder "setupScript" includes the script that we use for setting up fog computing systems, and for performing multiple experiments automatically.

The folder "dependencies" includes the pom.xml file which includes all the dependencies of the source code.