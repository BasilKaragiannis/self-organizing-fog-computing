# Data Sovereignty at the Edge of the Network
  
This repository includes supplementary material for a submission to IEEE ICFEC 2023 titled "Data Sovereignty at the Edge of the Network". The following folders are available:

- `prototype/`: Contains the code of a prototype implementation. The file prototype/README.md describes how to run the Eclipse Dataspace Connector. There is a tutorial for beginners at prototype/onboarding.md. The files of our implementation are in prototype/samples/projects/base-connector-icfec2023/ and utilize extensions which are implemented in prototype/extensions/custom-extensions/data-plane-http-with-transfer-metric. Finally, the main function that runs the experiments is prototype/extensions/custom-extensions/data-plane-http-with-transfer-metric/src/main/java/org/eclipse/edc/connector/dataplane/http/orchestrator/Main2.java.

 - `results/`: Contains an excel file with samples of results from various experiments regarding communication latency and hop count.