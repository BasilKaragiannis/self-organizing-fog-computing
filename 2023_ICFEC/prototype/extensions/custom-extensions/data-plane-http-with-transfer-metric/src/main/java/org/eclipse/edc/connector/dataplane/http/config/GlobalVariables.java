package org.eclipse.edc.connector.dataplane.http.config;

import java.util.ArrayList;
import java.util.Arrays;

public class GlobalVariables {
    //To hold the time when the connector starts reading the data
    public static long startTimer = 0;
    //To hold the time when the connector finishes writting the data
    public static long stopTimer = 0;
    //The IP of the source node
    public static String sourceIP = "";  
    //The IP of the sink node
    public static String sinkIP = "";
    //The external IP of this node. It is used in edge mode to count hops
    //in cloud mode, this is take from the sourcePath. In edge mode, the sourcePath is localhost
    public static String externalIp = "0.0.0.0";
    //switch between cloud and edge mode to change how hops are counted
    public static String mode = "cloud"; //either "cloud" or "edge" 
    //List of the IPs of the nodes
    public static final ArrayList<String> nodeIPs = new ArrayList<>(Arrays.asList(  "35.205.62.237",
                                                                                    "35.246.67.113",
                                                                                    "34.175.185.82",
                                                                                    "34.154.90.63",
                                                                                    "35.204.184.45",
                                                                                    "34.163.220.18",
                                                                                    "34.118.118.232"));
    //Statically configured hop count between nodes in cloud mode
    public static final int hopsCloud[][] = {   {0,34,33,32,35,32,32},
                                                {34,0,31,30,33,30,30},
                                                {33,31,0,29,32,29,29},
                                                {32,30,29,0,31,28,28},
                                                {35,33,32,31,0,31,31},
                                                {32,30,29,28,31,0,28},
                                                {32,30,29,28,31,28,0}
                                            };
    //Statically configured hop count between nodes in edge mode
    public static final int hopsEdge[][] = {{0,17,16,15,16,16,15},
                                            {17,0,15,16,18,14,16},
                                            {16,15,0,15,18,17,34},
                                            {15,16,15,0,17,15,34},
                                            {16,18,18,17,0,15,14},
                                            {16,14,17,15,15,0,16},
                                            {15,16,34,34,14,16,0}
                                        };

}

