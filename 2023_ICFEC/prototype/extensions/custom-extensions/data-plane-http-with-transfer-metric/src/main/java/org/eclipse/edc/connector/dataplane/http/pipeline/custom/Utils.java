package org.eclipse.edc.connector.dataplane.http.pipeline.custom;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.edc.connector.dataplane.http.config.GlobalVariables;

public class Utils {
    //To find the number of hops between source and sink in edge mode
    public static int getHopsEdge(String source, String sink){
        int senderNode = GlobalVariables.nodeIPs.indexOf(source);
        int receiverNode = GlobalVariables.nodeIPs.indexOf(sink);
        if(senderNode == -1 || receiverNode == -1){
            return -1;
        }
        return GlobalVariables.hopsEdge[senderNode][receiverNode];
    }
    //To find the number of hops between source and sink in cloud mode
    public static int getHopsCloud(String source, String sink){
        int senderNode = GlobalVariables.nodeIPs.indexOf(source);
        int receiverNode = GlobalVariables.nodeIPs.indexOf(sink);
        if(senderNode == -1 || receiverNode == -1){
            return -1;
        }
        return GlobalVariables.hopsCloud[senderNode][receiverNode];
    }
    //
    public static int getHops(String source, String sink, String mode){
        int hops = 0;
        if(mode.equals("cloud")){
            hops = getHopsCloud(source, sink);
        }else if(mode.equals("edge")){
            hops = getHopsEdge(source, sink);
        }
        return hops;
    }

    //
    public static void saveStats() throws IOException{
        //in edge mode, the source is localhost, so use external IP instead
        if(GlobalVariables.mode.equals("edge")){
            GlobalVariables.sourceIP = GlobalVariables.externalIp;
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter("stats.txt", true)); 
        writer.append(GlobalVariables.sourceIP + " " + GlobalVariables.sinkIP + " " + 
            (GlobalVariables.stopTimer - GlobalVariables.startTimer) + " " + 
            getHops(GlobalVariables.sourceIP, GlobalVariables.sinkIP, GlobalVariables.mode));
        writer.newLine();
        writer.close();
    }

    //
    public static String readStats() throws IOException{
        String file ="stats.txt";
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String fileString = "";
        String line = reader.readLine();
        while(line != null){
            fileString = fileString + line + "\n";
            line = reader.readLine();
        }
        reader.close();
        return fileString;
    }

    public static void resetStats() throws IOException{
        BufferedWriter writer = new BufferedWriter(new FileWriter("stats.txt")); 
        writer.write("");
        writer.close();
    }
}
