package org.eclipse.edc.connector.dataplane.http.orchestrator;

import java.util.ArrayList;
import java.util.Random;

public class Main2 {

	public static void main(String[] args) {

		// String[] modes = { "cloud", "edge" }; // cloud or edge
		String fileSize = "85b"; // used as file name when saving the stats
		String fileName = "testFile-1value.json";
		String fileLocation = "/home/ubuntu/Projects/Eclipse-Dataspace-Connector-Fork/samples/projects/base-connector-icfec2023/python-http-store/data-store/";
		int delay = 5000; // delay between transfers
		String savePath = "/home/ubuntu/Projects/Eclipse-Dataspace-Connector-Fork/samples/projects/base-connector-icfec2023/provider/";

		String cloudProviderConnector = "http://34.159.40.42"; // used only in cloud mode
		String cloudConsumerConnector = "http://34.159.195.247"; // used only in cloud mode
		String providerPassword = "password";
		String consumerPassword = "password";
		Random random = new Random(0); // random seed

		String[] nodes = {
				// "http://localhost",
				// "http://localhost" };

				"http://35.205.62.237", // belgium
				"http://35.246.67.113", // london
				"http://34.175.185.82", // madrid
				"http://34.154.90.63", // milan
				"http://35.204.184.45", // netherlands
				"http://34.163.220.18", // paris
				"http://34.118.118.232" }; // warsaw

		int size = 10; // number of repetitions for sending to each one of the other nodes
		int experiments = nodes.length * ((nodes.length - 1) * size); // total number of experiments

		// post mode for cloud
		Utils.sendPostRequest(cloudProviderConnector + ":8181/api/postMode/" + "cloud", "", "");
		// post mode for edge
		for (String node : nodes) {
			Utils.sendPostRequest(node + ":8181/api/postMode/" + "edge", "", "");
		}

		// reset stats for cloud
		Utils.sendGetRequest(cloudProviderConnector + ":8181/api/resetStats", "");
		// reset stats for edge, post external ip
		for (String node : nodes) {
			Utils.sendGetRequest(node + ":8181/api/resetStats", "");
			Utils.sendPostRequest(node + ":8181/api/postExternalIp/" + node.split("//")[1], "", "");
		}

		// generate list of trips, i.e., sourceIP destinationIP
		ArrayList<String> trips = Utils.generateTrips(random, size, nodes);

		// put file in provider store
		try {
			String file = Utils.readFile(fileLocation + fileName);
			for (String node : nodes) {
				Utils.sendPUTRequest(node + ":5008/putFile?name=" + fileName, file);
			}
		} catch (Exception e) {
			System.out.println("Problem loading file");
			e.printStackTrace();
			System.exit(0);
		}

		// register file in provider connector
		for (String node : nodes) {
			// register for cloud
			Utils.registerFile(cloudProviderConnector, node, ":5008/getFile?name=" + fileName, providerPassword);
			// register for edge
			Utils.registerFile(node, node, ":5008/getFile?name=" + fileName, providerPassword);
		}

		String contractIdCloud = "";
		String contractIdEdge = "";

		String previousSource = "";
		int sameSourceFlag = 0;

		// start tranfers
		for (int i = 0; i < trips.size(); i++) {
			System.out.println((i + 1) + "/" + trips.size() + ": ");

			String source = trips.get(i).split(" ")[0];
			String sink = trips.get(i).split(" ")[1];

			// check if the previous source is the same as this source. if it is, not need
			// to negotiate new contract
			if (previousSource.equals(source)) {
				sameSourceFlag = 1;
			} else {
				sameSourceFlag = 0;
			}
			previousSource = source;

			if (sameSourceFlag == 0) {
				// for cloud
				contractIdCloud = Utils.transferData(trips.get(i), cloudProviderConnector, cloudConsumerConnector,
						consumerPassword, fileName);
				// for edge
				contractIdEdge = Utils.transferData(trips.get(i), source, sink, consumerPassword, fileName);
			} else {
				// for cloud
				Utils.transferDataWithContract(contractIdCloud, trips.get(i), cloudProviderConnector,
						cloudConsumerConnector,
						consumerPassword, fileName);
				// for edge
				Utils.transferDataWithContract(contractIdEdge, trips.get(i), source, sink, consumerPassword,
						fileName);
			}
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
		System.out.println("********* all file transfers were requested *********");
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// gather data
		String dataCloud = "";
		String dataEdge = "";
		// for cloud
		dataCloud = Utils.getData(cloudProviderConnector);
		// for edge
		for (String node : nodes) {
			dataEdge = dataEdge + Utils.getData(node);
		}
		// save data
		Utils.saveFile(dataCloud, savePath + "cloud" + "-" + fileSize + "-" + experiments + ".txt");
		Utils.saveFile(dataEdge, savePath + "edge" + "-" + fileSize + "-" + experiments + ".txt");
		System.out.println("********* finished *********");
	}
}
