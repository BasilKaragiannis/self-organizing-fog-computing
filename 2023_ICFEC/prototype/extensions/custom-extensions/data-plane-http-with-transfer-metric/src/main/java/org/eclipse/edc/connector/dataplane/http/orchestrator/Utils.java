package org.eclipse.edc.connector.dataplane.http.orchestrator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

public class Utils {
	/**
	 * sends a GET request to targetUrl
	 * 
	 * @param targetUrl the address of the get request
	 * @return the response of the get request
	 */
	public static String sendGetRequest(String targetUrl, String password) {
		StringBuffer response = new StringBuffer();
		try {

			Thread.sleep(3000);

			HttpURLConnection connection = (HttpURLConnection) new URL(targetUrl).openConnection();
			connection.setRequestMethod("GET");
			connection.setConnectTimeout(10000);
			connection.setReadTimeout(10000);
			connection.setRequestProperty("Content-Type", "application/json");
			if (!password.equals("")) {
				connection.setRequestProperty("x-api-key", "password");
			}
			if (connection.getResponseCode() != 200) {
				throw new RuntimeException("GET Request to " + targetUrl +
						" failed with Error code : "
						+ connection.getResponseCode());
			}
			BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
					(connection.getInputStream())));
			String output;
			while ((output = responseBuffer.readLine()) != null) {
				response.append(output + "\n");
			}
			connection.disconnect();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("connectionTimeout. " + "Target URL: " + targetUrl);
		}
		return response.toString();
	}

	/**
	 * sends a POST request to targetUrl
	 * 
	 * @param targetUrl the address of the POST request
	 * @param input     a string to be sent through the POST request
	 * @return the HTTP code the response of the POST request
	 */
	public static String sendPostRequest(String targetUrl, String body, String password) {
		int responseCode = 0;
		String responseText = null;
		try {

			Thread.sleep(3000);

			URL url = new URL(targetUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(10000);
			connection.setReadTimeout(10000);
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			if (!password.equals("")) {
				connection.setRequestProperty("x-api-key", "password");
			}

			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(body);
			out.flush();
			out.close();

			responseCode = connection.getResponseCode();
			InputStream is = connection.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			responseText = br.readLine();
			connection.disconnect();
		} catch (Exception e) { // TODO: catch the thrown Exception! NEVER catch "Exception"
			// TODO: log instead of print
			System.out.println(e.getMessage());
			System.out.println("connectionTimeout. " + "Target URL: " + targetUrl);
		}
		return responseText;
	}

	/**
	 * sends a PUT request to targetUrl
	 * 
	 * @param targetUrl the address of the POST request
	 * @param input     a string to be sent through the POST request
	 * @return the HTTP code the response of the POST request
	 */
	public static String sendPUTRequest(String targetUrl, String body) {
		int responseCode = 0;
		String responseText = null;
		try {

			Thread.sleep(3000);

			URL url = new URL(targetUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(10000);
			connection.setReadTimeout(10000);
			connection.setRequestMethod("PUT");
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/octet-stream");

			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(body);
			out.flush();
			out.close();

			responseCode = connection.getResponseCode();
			InputStream is = connection.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			responseText = br.readLine();
			connection.disconnect();
		} catch (Exception e) { // TODO: catch the thrown Exception! NEVER catch "Exception"
			// TODO: log instead of print
			System.out.println(e.getMessage());
			System.out.println("connectionTimeout. " + "Target URL: " + targetUrl);
		}
		return responseText;
	}

	public static String[][] generateRandomTrips(Random random, int size, String[] nodes) {
		String trips[][] = new String[size][2];
		for (int i = 0; i < size; i++) {
			trips[i][0] = nodes[random.nextInt(nodes.length)];
			trips[i][1] = nodes[random.nextInt(nodes.length)];
			while (trips[i][0] == trips[i][1]) {
				trips[i][1] = nodes[random.nextInt(nodes.length - 1)];
			}
		}
		return trips;
	}

	public static ArrayList<String> generateTrips(Random random, int size, String[] nodes) {
		ArrayList<String> trips = new ArrayList<String>();

		for (int n = 0; n < nodes.length; n++) {
			for (int i = 0; i < nodes.length; i++) {
				if (n != i) {
					for (int j = 0; j < size; j++) {
						trips.add(nodes[n] + " " + nodes[i]);
					}
				}
			}
		}
		// for (int i = 0; i < trips.size(); i++) {
		// 	System.out.println(trips.get(i));
		// }
		return trips;
	}

	public static String readFile(String path) {
		BufferedReader reader;
		String file = "";
		try {
			reader = new BufferedReader(new FileReader(path));
			String line = reader.readLine();
			while (line != null) {
				file = file + line + "\n";
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file;
	}

	public static void registerFile(String provider, String source, String filePath, String password) {
		String assetId = source.split("//")[1];
		String baseUrl = source + filePath; // "http://localhost:5000/getFile?name=testFile.json";
		String fileName = baseUrl.split("=")[1];
		String createAssetBody = "{\"asset\":{\"properties\":{\"asset:prop:name\":\"" + fileName +
				"\",\"asset:prop:description\":\"Genericdataset\",\"asset:prop:contenttype\":\"application/json\"" +
				",\"asset:prop:version\":\"1.0\",\"asset:prop:id\":\"" + assetId + "\"}},\"dataAddress\"" +
				":{\"properties\":{\"name\":\"\",\"type\":\"HttpData\",\"baseUrl\":\"" + baseUrl + "\",\"" +
				"contentType\":\"application/json\",\"transferInOneGo\":\"true\"}}}";

		sendPostRequest(provider + ":8585/management/assets", createAssetBody, password);

		String policyId = assetId;
		String createPolicyBody = "{\"id\":\"" + policyId + "\",\"policy\":{\"permissions\":[{\"edctype\"" +
				":\"dataspaceconnector:permission\",\"target\":\"" + assetId + "\",\"action\":{\"type\":\"USE\"" +
				"}}],\"target\":\"" + assetId + "\"}}";
		sendPostRequest(provider + ":8585/management/policydefinitions", createPolicyBody, password);

		String contractId = assetId;
		String createContractBody = "{\"accessPolicyId\":\"" + policyId + "\",\"contractPolicyId\":\"" +
				contractId + "\",\"criteria\":[{\"operandLeft\":\"asset:prop:id\",\"operator\":\"=\",\"" +
				"operandRight\":\"" + assetId + "\"}],\"id\":\"" + assetId + "\"}";
		sendPostRequest(provider + ":8585/management/contractdefinitions", createContractBody, password);
	}

	public static String transferData(String trip, String providerConnector, String consumerconnector,
			String consumerPassword, String fileName) {
		String source = trip.split(" ")[0];
		// String sink = trip.split(" ")[1];
		String assetId = source.split("//")[1];;

		// get catalog
		String getCatalogBody = "{\"providerUrl\":\"" + providerConnector + ":8282/api/v1/ids/data\"}";
		String catalog = Utils.sendPostRequest(consumerconnector + ":9595/management/catalog/request", getCatalogBody,
				consumerPassword);

		// negotiate contract
		String contractOfferId = assetId + ":" + catalog.split(assetId + ":", 2)[1].split("\"")[0];
		String contractNegotiationBody = "{\"connectorId\":\"provider\",\"protocol\":\"ids-multipart\",\"" +
				"connectorAddress\":\"" + providerConnector + ":8282/api/v1/ids/data\",\"offer\":{\"offerId\"" +
				":\"" + contractOfferId + "\",\"assetId\":\"" + assetId + "\",\"policy\":{\"permissions\"" +
				":[{\"edctype\":\"dataspaceconnector:permission\",\"uid\":null,\"target\":\"" + assetId +
				"\",\"action\":{\"type\":\"USE\",\"includedIn\":null,\"constraint\":null},\"assignee\":null" +
				",\"assigner\":null,\"constraints\":[],\"duties\":[]}],\"prohibitions\":[],\"obligations\":[]" +
				",\"extensibleProperties\":{},\"inheritsFrom\":null,\"assigner\":null,\"assignee\":null,\"" +
				"target\":null,\"@type\":{\"@policytype\":\"set\"}},\"asset\":{\"properties\":{\"ids:byteS" +
				"ize\":null,\"asset:prop:id\":\"" + assetId + "\",\"ids:fileName\":null}}}}";
		String negotiationResponse = Utils.sendPostRequest(consumerconnector + ":9595/management/contractnegotiations",
				contractNegotiationBody, consumerPassword);

		// get contract id
		String negotiationId = negotiationResponse.split("\"id\":\"", 2)[1].split("\"", 2)[0];
		String contractId = "null";
		int firstTimeFlag = 0;
		while (contractId.equals("null")) {
			try {
				if (firstTimeFlag == 0) {
					Thread.sleep(5000);
					firstTimeFlag = 1;
				} else {
					Thread.sleep(2000);
					System.out.println("The provider is taking a long time to approve the contract agreement");
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			String contractIdResponse = Utils.sendGetRequest(consumerconnector +
					":9595/management/contractnegotiations/" + negotiationId, consumerPassword);
			if (!contractIdResponse.split(",")[2].contains("null")) {
				contractId = contractIdResponse.split("\"contractAgreementId\":\"", 2)[1].split("\"", 2)[0];
				System.out.println(providerConnector + ": Contract agreement approved");
			}
		}
		// start the transfer process
		transferDataWithContract(contractId, trip, providerConnector, consumerconnector,
				consumerPassword, fileName);

		return contractId;
	}

	public static void transferDataWithContract(String contractId, String trip, String providerConnector,
			String consumerconnector,
			String consumerPassword, String fileName) {

		String source = trip.split(" ")[0];
		String sink = trip.split(" ")[1];
		String assetId = source.split("//")[1];;

		// start the transfer process
		String baseUrl = sink + ":5009/putFile?name=" + fileName;
		String transferProcessBody = "{\"connectorAddress\":\"" + providerConnector + ":8282/api/v1/ids/data\",\"" +
				"protocol\":\"ids-multipart\",\"connectorId\":\"consumer\",\"assetId\":\"" + assetId
				+ "\",\"contractId\"" +
				":\"" + contractId + "\",\"dataDestination\":{\"properties\":{\"baseUrl\":\"" + baseUrl
				+ "\",\"method\"" +
				":\"PUT\",\"name\":\"\",\"type\":\"HttpData\",\"contentType\":\"application/octet-stream\",\"" +
				"transferInOneGo\":true,\"header:x-ms-blob-type\":\"BlockBlob\"}},\"managedResources\":false,\"" +
				"transferType\":{\"contentType\":\"application/octet-stream\",\"isFinite\":true}}";
		String transferProcessResponse = Utils.sendPostRequest(consumerconnector + ":9595/management/transferprocess",
				transferProcessBody, consumerPassword);
	}

	public static String getData(String provider) {
		return sendGetRequest( provider + ":8181/api/getStats", "");
	}

	public static void saveFile(String data, String path) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(path));
			writer.write(data);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
