/*
 *  Copyright (c) 2021 Microsoft Corporation
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       Microsoft Corporation - Initial implementation
 *
 */

package org.eclipse.edc.connector.dataplane.http.config;


import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import java.io.IOException;

import org.eclipse.edc.connector.dataplane.http.pipeline.custom.Utils;
import org.eclipse.edc.spi.monitor.Monitor;

@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path("/")
public class ConfigApiController {

    private final Monitor monitor;

    public ConfigApiController(Monitor monitor) {
        this.monitor = monitor;
    }

    @GET
    @Path("getExternalIp")
    public String getExternalIp() {
        monitor.info("Received an getExternalIp GET request");
        return "{\"response\":\"" + GlobalVariables.externalIp + "\"}";
    }

    @POST
    @Path("postExternalIp/{externalIp}")
    public String postExternalIp(@PathParam("externalIp") String externalIp) {
        monitor.info("Received a /postExternalIp POST request");
        GlobalVariables.externalIp = externalIp;
        return "{\"response\":\"" + GlobalVariables.externalIp + "\"}";
    }


    @GET
    @Path("getMode")
    public String getMode() {
        monitor.info("Received a getMode GET request");
        return "{\"response\":\"" + GlobalVariables.mode + "\"}";
    }

    @POST
    @Path("postMode/{mode}")
    public String postMode(@PathParam("mode") String mode) {
        monitor.info("Received a /postMode POST request");
        GlobalVariables.mode = mode;
        return "{\"response\":\"" + GlobalVariables.mode + "\"}";
    }

    @GET
    @Path("getStats")
    public String getStats() throws IOException {
        monitor.info("Received a /getStats GET request");
        return Utils.readStats();
    }
    @GET
    @Path("resetStats")
    public String resetStats() throws IOException {
        monitor.info("Received a /resetStats GET request");
        Utils.resetStats();
        return "{\"response\":\"" + "Stats file is now empty" + "\"}";
    }
}
