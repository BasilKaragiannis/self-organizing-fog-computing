import os
from flask import Flask, flash, request, Response, send_file
from werkzeug.utils import secure_filename
import json

app = Flask(__name__)

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'json'}
app.config['UPLOAD_FOLDER'] = 'data-store'

@app.route('/')
def hello():
    return 'Hello. <br>Try /putFile to upload a file. <br>Try /getFile to download a file.'

@app.route("/putFile", methods = ["PUT", "POST"])
def putFile():
    if request.method == 'PUT':
        if request.content_type == "application/octet-stream":
            data = request.get_data()
            args = request.args
            filename = args.get("name")
            if filename == None:
                return Response("File name is missing", status=200)
            if filename == '':
                return Response("File name is empty", status=200)
            newfilename =filename
            # #to avoid overwrite
            # count = 0
            # while os.path.exists("./" + newfilename):
            #     count = count + 1
            #     newfilename = filename.split(".")[0] + "-" + str(count) + "." + filename.split(".")[1]
            file = open(os.path.join(app.config['UPLOAD_FOLDER'], newfilename), "wb")
            file. write(data)
            file. close()
            return Response("File saved", status=200)
        return Response("application/octet-stream is needed as content type", status=200)
    return Response("Only PUT is allowed", status=200)

    # Using a form
    #     # check if the post request has the file part
    #     if 'file' not in request.files:
    #         return Response("No file found", status=200)
    #     file = request.files['file']
    #     if file.filename == '':
    #         return Response("No file name found", status=200)
    #     if allowed_file(file.filename) == False:
    #         return Response("File type not allowed", status=200)
    #     if file and allowed_file(file.filename):
    #         filename = secure_filename(file.filename)
    #         count = 0
    #         newfilename =filename
    #         while os.path.exists("./" + newfilename):
    #             count = count + 1
    #             newfilename = filename.split(".")[0] + "-" + str(count) + "." + filename.split(".")[1]
    #         file.save(os.path.join(app.config['UPLOAD_FOLDER'], newfilename))
    #         return Response("File saved", status=200)
    # return Response("Only PUT is allowed", status=200)

@app.route("/getFile", methods = ["GET", "POST"])
def getFile():
    if request.method == 'GET':
        args = request.args
        filename = args.get("name")
        if filename == None:
            return Response("File name is missing", status=200)
        if filename == '':
            return Response("File name is empty", status=200)
        if os.path.exists(os.path.join(app.config['UPLOAD_FOLDER'], filename)) == False:
            return Response("File does not exist", status=200)
        return send_file(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    return Response("Only GET is allowed", status=200)


# utils
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

if __name__ == "__main__":
    # app.run(host="127.0.0.1", port=5000)
    app.run(host="0.0.0.0", port=5000)
