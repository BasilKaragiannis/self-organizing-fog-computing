/*
 *  Copyright (c) 2020, 2021 Microsoft Corporation
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       Microsoft Corporation - initial API and implementation
 *       Fraunhofer Institute for Software and Systems Engineering - added dependencies
 *       ZF Friedrichshafen AG - add dependency
 *
 */

plugins {
    `java-library`
    id("application")
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

val jupiterVersion: String by project
val rsApi: String by project
val openTelemetryVersion: String by project
val nimbusVersion: String by project
val jerseyVersion: String by project

dependencies {
    // implementation(project(":core"))

    // implementation(project(":extensions:common:api:api-observability"))

    // implementation(project(":extensions:common:vault:vault-filesystem"))
    // implementation(project(":extensions:common:configuration:configuration-filesystem"))

    // implementation(project(":extensions:common:iam:iam-mock"))


    // implementation(project(":extensions:common:http"))

    // implementation(project(":extensions:common:auth:auth-tokenbased"))
    // implementation(project(":extensions:control-plane:api:data-management-api"))

    // implementation(project(":data-protocols:ids")) {
    //     exclude("org.eclipse.dataspaceconnector","ids-token-validation")
    // }

    // implementation(project(":extensions:data-plane:data-plane-api"))
    // implementation(project(":extensions:data-plane-selector:data-plane-selector-client"))
    // implementation(project(":core:data-plane-selector:data-plane-selector-core"))
    // implementation(project(":extensions:data-plane-selector:store"))
    // implementation(project(":core:data-plane:data-plane-framework"))
    // implementation(project(":extensions:data-plane:data-plane-http"))

    // //instead of implementation(project(":extensions:data-plane:data-plane-spi"))
    // implementation(project(":spi:data-plane:data-plane-spi"))
    // implementation(project(":core:common:util"))

    // implementation(project(":spi:common:core-spi"))
    
    // implementation("io.opentelemetry:opentelemetry-extension-annotations:${openTelemetryVersion}")
    // implementation("jakarta.ws.rs:jakarta.ws.rs-api:${rsApi}")

    // //////////////////////////////////////////////////////////

    // implementation(project(":core"))
    // implementation(project(":core:control-plane:control-plane-core"))

    // implementation("org.glassfish.jersey.core:jersey-server:${jerseyVersion}")
    // implementation("org.glassfish.jersey.containers:jersey-container-servlet-core:${jerseyVersion}")
    // implementation("org.glassfish.jersey.core:jersey-common:${jerseyVersion}")
    // implementation("org.glassfish.jersey.media:jersey-media-json-jackson:${jerseyVersion}")
    // implementation("org.glassfish.jersey.media:jersey-media-multipart:${jerseyVersion}")

    // implementation(project(":data-protocols:ids:ids-jsonld-serdes"))





    //     implementation(project(":extensions:common:iam:iam-mock"))
    // //    implementation(project(":samples:other:file-transfer-http-to-http:api-mock"))

    // implementation("com.nimbusds:nimbus-jose-jwt:${nimbusVersion}")

    // implementation(project(":extensions:common:http"))

    // implementation(project(":extensions:common:auth:auth-tokenbased"))

    // implementation(project(":data-protocols:ids")) {
    //     exclude("org.eclipse.dataspaceconnector","ids-token-validation")
    // }

    // implementation(project(":extensions:data-plane:data-plane-api"))
    // implementation(project(":core:data-plane-selector:data-plane-selector-core"))
    // implementation(project(":core:data-plane:data-plane-framework"))
    // implementation(project(":extensions:data-plane:data-plane-http"))

    // implementation(project(":spi:common:core-spi"))

    // implementation("io.opentelemetry:opentelemetry-extension-annotations:${openTelemetryVersion}")

    // implementation(project(":spi:data-plane"))

    // implementation("jakarta.ws.rs:jakarta.ws.rs-api:${rsApi}")

    // //////////////////////////////////////////////////////////


    //control plane core, also boot (baseRuntime)
    implementation(project(":core:control-plane:control-plane-core"))
    //observability api
    implementation(project(":extensions:common:api:api-observability"))
    //config file from filesystem
    implementation(project(":extensions:common:configuration:configuration-filesystem"))
    //dummy authority
    implementation(project(":extensions:common:iam:iam-mock"))
    //token-based authontication (for management api)
    implementation(project(":extensions:common:auth:auth-tokenbased"))
    //control plane api (i.e., the management api)
    implementation(project(":extensions:control-plane:api:management-api"))
    //ids protocol for connector to connector communication
    implementation(project(":data-protocols:ids"))
    //data plane with http
/*
    implementation(project(":extensions:data-plane:data-plane-api"))
    implementation(project(":extensions:data-plane-selector:data-plane-selector-client"))
    implementation(project(":core:data-plane-selector:data-plane-selector-core"))
    implementation(project(":core:data-plane:data-plane-framework"))
    implementation(project(":extensions:data-plane:data-plane-http"))
    implementation(project(":spi:common:core-spi"))
    implementation(project(":spi:data-plane"))
    implementation(project(":extensions:control-plane:data-plane-transfer"))*/

 
    implementation(project(":extensions:data-plane:data-plane-http"))
    implementation(project(":core:data-plane:data-plane-core"))
    implementation(project(":spi:data-plane:data-plane-spi"))
    implementation(project(":spi:control-plane:transfer-spi"))
    implementation(project(":extensions:control-plane:data-plane-transfer:data-plane-transfer-client"))

    /*implementation(project(":extensions:control-plane:data-plane-transfer"))
    implementation(project(":extensions:data-plane-selector:data-plane-selector-client"))
    implementation(project(":core:data-plane-selector:data-plane-selector-core"))  */

    
    
//    implementation(project(":extensions:data-plane:data-plane-api"))
//    implementation(project(":extensions:common:vault:vault-filesystem"))

    //requires token validation setting edc.dataplane.token.validation.endpoint
//  implementation(project(":extensions:data-plane:data-plane-api"))

//   implementation(project(":spi:data-plane:data-plane-spi"))


// implementation(project(":extensions:common:vault:vault-filesystem"))

//     implementation(project(":core:data-plane:data-plane-core"))
//     implementation(project(":extensions:data-plane:data-plane-http"))
//     implementation(project(":extensions:data-plane:data-plane-api"))
//     implementation(project(":extensions:common:vault:vault-filesystem"))

//              
}

application {
    mainClass.set("org.eclipse.edc.boot.system.runtime.BaseRuntime")
}

tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
    exclude("**/pom.properties", "**/pom.xm")
    mergeServiceFiles()
    archiveFileName.set("provider.jar")
}