/*
 *  Copyright (c) 2020, 2021 Microsoft Corporation
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       Microsoft Corporation - initial API and implementation
 *       Fraunhofer Institute for Software and Systems Engineering - added dependencies
 *       ZF Friedrichshafen AG - add dependency
 *
 */

plugins {
    `java-library`
    id("application")
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

val jupiterVersion: String by project
val rsApi: String by project
val openTelemetryVersion: String by project
val nimbusVersion: String by project
val jerseyVersion: String by project

dependencies {
        //control plane core, also boot (baseRuntime)
    implementation(project(":core:control-plane:control-plane-core"))
    //observability api
    implementation(project(":extensions:common:api:api-observability"))
    //config file from filesystem
    implementation(project(":extensions:common:configuration:configuration-filesystem"))
    //dummy authority
    implementation(project(":extensions:common:iam:iam-mock"))
    //token-based authontication (for management api)
    implementation(project(":extensions:common:auth:auth-tokenbased"))
    //control plane api (i.e., the management api)
    implementation(project(":extensions:control-plane:api:management-api"))
    //ids protocol for connector to connector communication
    implementation(project(":data-protocols:ids"))
    //data plane with http

    implementation(project(":extensions:data-plane:data-plane-http"))
    implementation(project(":core:data-plane:data-plane-core"))
    implementation(project(":spi:data-plane:data-plane-spi"))
}

application {
    mainClass.set("org.eclipse.edc.boot.system.runtime.BaseRuntime")
}

tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
    exclude("**/pom.properties", "**/pom.xm")
    mergeServiceFiles()
    archiveFileName.set("consumer.jar")
}