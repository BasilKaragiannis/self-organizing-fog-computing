
FROM python:3.10-slim-buster

WORKDIR /app

COPY ./samples/projects/base-connector-icfec2023/python-http-store/http-store.py http-store.py
COPY ./samples/projects/base-connector-icfec2023/python-http-store/requirements.txt requirements.txt
COPY ./samples/projects/base-connector-icfec2023/python-http-store/data-store /app/data-store


RUN pip install -r requirements.txt

EXPOSE 5000

CMD [ "python", "http-store.py"]