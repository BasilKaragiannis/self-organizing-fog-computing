#
#  Copyright (c) 2021 Daimler TSS GmbH
#
#  This program and the accompanying materials are made available under the
#  terms of the Apache License, Version 2.0 which is available at
#  https://www.apache.org/licenses/LICENSE-2.0
#
#  SPDX-License-Identifier: Apache-2.0
#
#  Contributors:
#       Daimler TSS GmbH - Initial API and Implementation
#       Fraunhofer Institute for Software and Systems Engineering - added environment variables
#
#

FROM gradle:7.4.1-jdk17 AS build

COPY --chown=gradle:gradle . /home/gradle/project/
WORKDIR /home/gradle/project/
RUN gradle :samples:projects:base-connector-icfec2023:consumer:shadowJar --no-daemon

FROM openjdk:17-slim

WORKDIR /app
COPY --from=build /home/gradle/project/samples/projects/base-connector-icfec2023/consumer/build/libs/consumer.jar /app
COPY --from=build /home/gradle/project/samples/projects/base-connector-icfec2023/consumer/dataspaceconnector-configuration.properties /app

#Override properties
#ENV WEB_HTTP_PORT=9595
#ENV EDC_FS_CONFIG=./config.properties

EXPOSE 9191
EXPOSE 9192
EXPOSE 9292
EXPOSE 9393
EXPOSE 9494
EXPOSE 9595

ENTRYPOINT java \
    -Djava.security.edg=file:/dev/.urandom \
    -Dedc.ids.id="urn:connector:edc-connector-24" \
    -Dedc.ids.title="Eclipse Dataspace Connector" \
    -Dedc.ids.description="Eclipse Dataspace Connector with IDS extensions" \
    -Dedc.ids.maintainer="https://example.maintainer.com" \
    -Dedc.ids.curator="https://example.maintainer.com" \
    -jar consumer.jar
